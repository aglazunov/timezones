# Time Zones App

## Deployed version

You can immediately check out deployed production version at [timezones.glazunov.eu](https://timezones.glazunov.eu).
There are three users preconfigured, one for each of the roles (basic user, manager, administrator):

- user@example.com / userP@$$
- manager@example.com / managerP@$$
- admin@example.com / adminP@$$

## Running the app

### Prerequisites

- nodejs
- Mongo DB
- yarn optional, all further commands will be given assuming yarn

### Starting backend

```
cd backend
yarn
yarn start
```

Will launch node on port 3600. This is changeable in `backend/common/config/env.config`. Also, change to the same port in `web/src/setupProxy.js`.

Note: there are no default users created for the app. You'll be able to sign up through either UI or REST API but you will get a regular user's permissions. I've intentionally left a backdoor for easier testing. If you create a user with email equal to either manager@example.com or admin@example.com you'll get corresponding permissions.

To give yourself a user manager or admin rights you'll need to update the database entry. For this you'll need a user ID (can be looked at the API response). Then you'll need to run `mongo timezones` from shell and enter the following in the console (replace <ID> and <LEVEL> with values):

`db.users.update({"_id" : ObjectId("<ID>")},{$set:{"permissionLevel":<LEVEL>>}})`

Use the following for the <LEVEL>

- 3 for basic user
- 43 for user manager
- 47 for admin

### Starting front end (dev server)

```
cd web
yarn
yarn start
```

Will launch dev node server on port 3000.

## Repo Structure

The app consists of two parts:

- Front end (lives in folder `web`) - a react-based SPA
- Backend (lives in folder `backend`) - a nodejs-based backend that utilizes mongodb

## Running tests

### REST API tests

REST Api tests can be run from `backend` folder with the following command:

`yarn test`

They are based on nodejs and `supertest` library which launches its instance of the express server and allows for easy population of test data.

### Frontend unit tests

`web` folder contains a handful of unit tests which can be run with

`yarn test`

### Storybook

Storybook is used for visual testing of some components. It can be launched from `web` directory with this command:

`yarn run storybook`

### End to end tests

There's a basic workflow test added that will sign up a user, search and add time zones, view and delete a time zone, log user out, logs in again and deletes itself.

`yarn run cypress open` (with GUI)

`yarn run cypress run` (headless mode)

By default, it tests against localhost running on port 3000. Changeable in `web/cypress.json`.
