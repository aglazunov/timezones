# Thoughts on the nature of the app

At a first glance at the requirements I imagined world clock in hotel lobbies, or rather its digital version
with name of the city, its current time, and of course dark or light background depending on whether it's night or day. After all, I'm a UI developer. Though this has some caveats.

An easy time zone app is not as straightforward as, say, an easy app dealing with customers, their accounts and orders.
There is something external here – a list of time zones – which is often perceived constant except it's not. This can be illustrated on the example of how my home time zone of Europe/Minsk.
When I was around 8, my dad got us a new fax machine – fiddling around its settings I discovered that I need to set up the time zone to Athens/Cairo – we used to have the same time with aforementioned cities - both in winter (GMT+2) and in summer (GMT+3).
Now it's no longer the case – while Athens is still +2/+3, Cairo is +2/+2 and my home city Minsk is +3/+3. Belarus has undergone two changes involving time zone switch and DST cancelation. It also happens that countries are being split or united, new time zones are being created and older ones deprecated. Civil time history is a large topic on its own and can be followed in detail here.<sup>[[1]]</sup>

We'll be using the well-organized and maintained IANA tz database<sup>[[2]]</sup> as a source of truth for the time zones. Moreover, there is a JavaScript library tzdb<sup>[[3]]</sup> built on top of this data that provides ready to use object structures and is also regularly maintained and auto-updated.

Ability to add light/dark background feature implies that we will be able to figure out sunrise and sunset times at current day, which in turn will require geographic coordinates for a given city. As we'll be giving user an ability to type in anything in the city field, this will be outside of scope of this project.

# User flow

## Adding a timezone

User is presented with the list of all available time zones (317 at time of writing). Because adding a time zone will be the first feature of the app that will be used after log in, special attention must be paid developing an easy flow through it. We'll be providing a search feature that will be auto-activated (the input will be focused by default).

The search will be case-insensitive and filter the time zones using a few properties (like name, alternative name, abbreviation, country name). The idea is that user can think of the time zone in several ways. In case of, e.g. Europe/Berlin, they might think of it as Berlin time, CET or Germany time. So we need to give an ability to find this time zone when they start to type any of this.

We must consider another way of adding which is simply scrolling through the list without typing in the search term. To cover this use case we'll make the time zone list take almost the whole vertical space and make the items compact, ideally one line. This means we'll not display additional information such as alternative zone name and country in the list. For the space reasons we decide to display only some of the properties (name and abbreviation - let's call them major properties). Additional properties that we'll search on will be called auxiliary properties for the sake of this argumentation.

Potential misunderstanding might arise when the search term hasn't matched any major properties but has matched an auxiliary one. Using the previous example of Europe/Minsk time zone, the database confusingly has its alternative name as <i>Moscow Time</i>. So the user entering Moscow in search, will see both Moscow and Minsk in the filtered list. The solution here is to display only major properties for the time zone by default and additionally display any auxiliary properties that got matched by the search. Moreover, highlighting of the matched parts will help the user to select the right time zone even easier.

[1]: https://www.iana.org/time-zones
[2]: https://en.wikipedia.org/wiki/Tz_database
[3]: https://github.com/vvo/tzdb
