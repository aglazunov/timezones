import mongoose from 'mongoose';

import users from './users.json';
import timeZones from './timeZones.json';

const firstNames = [
  'James',
  'John',
  'Robert',
  'Michael',
  'William',
  'David',
  'Richard',
  'Joseph',
  'Charles',
  'Thomas',
  'Mary',
  'Patricia',
  'Jennifer',
  'Elizabeth',
  'Linda',
  'Barbara',
  'Susan',
  'Margaret',
  'Jessica',
  'Sarah',
];

const lastNames = [
  'Smith',
  'Johnson',
  'Williams',
  'Brown',
  'Jones',
  'Miller',
  'Davis',
  'Garcia',
  'Rodriguez',
  'Wilson',
];

const randomItem = (arr) => Math.floor(Math.random() * arr.length);

export const defaultTestUser = {
  firstName: 'Amy',
  lastName: 'Winehouse',
  email: 'amy@wineho.us',
  password: 'b4ck2bl4ck',
};

export const testUsers = [
  defaultTestUser,
  {
    firstName: 'John',
    lastName: 'Lennon',
    email: 'john@lenn.on',
    password: 'im4gine',
  },
  {
    firstName: 'Pete',
    lastName: 'Doherty',
    email: 'pete@doher.ty',
    password: '4nthems4doomedUth',
  },
];

export const generateRandomUser = () => ({
  firstName: randomItem(firstNames),
  lastName: randomItem(lastNames),
  email: `test${Math.round(Math.random() * 10000)}@test.com`,
  password: 'testpwd',
});

const replaceIds = (entry) => ({
  ...entry,
  _id: mongoose.Types.ObjectId(entry._id.$oid),
});

export const testData = {
  users: users.map(replaceIds),
  timeZones: timeZones.map(replaceIds),
};

export const credentials = {
  Basic: {
    email: 'user@example.com',
    password: 'userP@$$',
  },
  UserManager: {
    email: 'manager@example.com',
    password: 'managerP@$$',
  },
  Admin: {
    email: 'admin@example.com',
    password: 'adminP@$$',
  },
};

export const userIds = {
  Basic: '601996a9f2b574034d7a655f',
  UserManager: '601996bff2b574034d7a6560',
  Admin: '601996eff2b574034d7a6561',
};
