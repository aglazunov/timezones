import supertest from 'supertest';
import app from '../../app.mjs';
import { setupDB } from '../../setupTests.mjs';
import { credentials, testData, userIds } from '../testData/testData';
import { TestOutcomes } from '../testData/util';

setupDB(undefined, true);
const request = supertest(app);

const userTimeZones = Object.entries(userIds).reduce((acc, [userAlias, userId]) => {
  acc[userAlias] = testData.timeZones.filter((timeZone) => timeZone.userId === userId);

  return acc;
}, {});

it("doesn't allow unauthorized access to /timezones", async () => {
  expect.assertions(1);
  const response = await request.get('/api/timezones');

  expect(response.status).toBe(401);
});

it("doesn't allow unauthorized access to user's timezones", async () => {
  expect.assertions(1);
  const response = await request.get(`/api/users/${userIds.Basic}/timezones`);

  expect(response.status).toBe(401);
});

it("doesn't allow unauthorized access to a specific timezone", async () => {
  expect.assertions(1);
  const response = await request.get(`/api/timezones/${testData.timeZones[0]._id.toHexString()}`);

  expect(response.status).toBe(401);
});

const expectRud = async (userSubject, userObject, expectedOutcome = TestOutcomes.Success) => {
  const successExpected = expectedOutcome === TestOutcomes.Success;
  expect.assertions(11);

  const expectedStatus = successExpected ? 200 : 404;
  const expectedStatusForCreate = successExpected ? 201 : 503;
  const expectedStatusForDelete = successExpected ? 204 : 404;
  // after own user is deleted, the authorization is expected to become invalid
  const expectedStatusAfterDelete = 404;

  let response = await request.post('/api/login').send(credentials[userSubject]);
  const { accessToken } = response.body;
  expect(typeof accessToken).toBe('string');

  response = await request
    .post(`/api/users/${userIds[userObject]}/timezones`)
    .send({
      name: 'Asia/Hong_Kong',
      city: 'Hong Kong',
    })
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatusForCreate);

  response = await request
    .get(`/api/users/${userIds[userObject]}/timezones`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatus);
  expect(response.body).toBeInstanceOf(Array);
  expect(response.body.length).toBe(userTimeZones[userObject].length + 1);

  const sampleTimeZoneId = userTimeZones[userObject][0]._id.toHexString();

  response = await request
    .patch(`/api/timezones/${sampleTimeZoneId}`)
    .send({
      city: 'The secret city',
    })
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatus);
  expect(response.body.city).toBe(successExpected ? 'The secret city' : undefined);

  response = await request
    .delete(`/api/timezones/${sampleTimeZoneId}`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatusForDelete);

  response = await request
    .get(`/api/timezones/${sampleTimeZoneId}`)
    .set('Authorization', `Bearer ${accessToken}`);
  expect(response.status).toBe(expectedStatusAfterDelete);

  response = await request
    .get(`/api/users/${userIds[userObject]}/timezones`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatus);
  expect(response.body.length).toBe(userTimeZones[userObject].length);
};

it('allows CRUD on own time zones to basic user', async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it('allows CRUD on own time zones to user manager', async () => {
  await expectRud('UserManager', 'UserManager', TestOutcomes.Success);
});

it('allows CRUD on own time zones to admin', async () => {
  await expectRud('Admin', 'Admin', TestOutcomes.Success);
});

it("denies CRUD on user manager's time zones to basic user", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it("denies CRUD on admin's time zones to basic user", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it("denies CRUD on basic user's time zones to user manager", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it("denies CRUD on admin's time zones to user manager", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it("allows CRUD on basic user's time zones to admin", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it("allows CRUD on user manager's time zones to admin", async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});
