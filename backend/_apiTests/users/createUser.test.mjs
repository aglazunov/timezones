/* eslint-disable  import/no-extraneous-dependencies */

import supertest from 'supertest';
import app from '../../app.mjs';

import { setupDB } from '../../setupTests.mjs';
import { generateRandomUser } from '../testData/testData.mjs';
import { basicPermissions } from '../../common/config/permissions.mjs';

setupDB();
const request = supertest(app);

it('creates a user', async () => {
  expect.assertions(2);
  const response = await request.post('/api/users').send(generateRandomUser());

  expect(response.status).toBe(201);
  expect(response.body).toHaveProperty('id');
});

it('creates a user with basic permissions', async () => {
  expect.assertions(1);

  const generatedUser = generateRandomUser();
  const response = await request.post('/api/users/createAndLogIn').send(generatedUser);
  const { user } = response.body;

  expect(user.permissionLevel).toEqual(basicPermissions);
});

it('creates a user with basic permissions, even when different permissions are provided', async () => {
  expect.assertions(1);

  const generatedUser = { ...generateRandomUser(), permissionLevel: 1000 };
  const response = await request.post('/api/users/createAndLogIn').send(generatedUser);
  const { user } = response.body;

  expect(user.permissionLevel).toEqual(basicPermissions);
});

it("doesn't allow user creation without an email", async () => {
  expect.assertions(1);

  const user = generateRandomUser();
  delete user.email;
  const response = await request.post('/api/users').send(user);

  expect(response.status).toEqual(400);
});

it("doesn't allow user creation with an invalid email", async () => {
  expect.assertions(1);

  const user = generateRandomUser();
  user.email = 'invalidemail.com';
  const response = await request.post('/api/users').send(user);

  expect(response.status).toEqual(400);
});

it("doesn't allow user creation without a password", async () => {
  expect.assertions(1);

  const user = generateRandomUser();
  delete user.password;
  const response = await request.post('/api/users').send(user);

  expect(response.status).toEqual(400);
});

it("doesn't allow user creation with an empty password", async () => {
  expect.assertions(1);

  const user = generateRandomUser();
  user.password = '';
  const response = await request.post('/api/users').send(user);

  expect(response.status).toEqual(400);
  expect(response.body.to);
});
