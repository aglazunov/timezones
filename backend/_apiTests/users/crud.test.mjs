/* eslint-disable  import/no-extraneous-dependencies */

import supertest from 'supertest';
import { credentials, userIds } from '../testData/testData.mjs';
import app from '../../app.mjs';
import { setupDB } from '../../setupTests.mjs';
import { TestOutcomes } from '../testData/util';

setupDB(undefined, true);
const request = supertest(app);

it("doesn't allow unauthorized access to /users", async () => {
  expect.assertions(1);
  const response = await request.get('/api/users');

  expect(response.status).toBe(401);
});

const expectRud = async (userSubject, userObject, expectedOutcome = TestOutcomes.Success) => {
  const successExpected = expectedOutcome === TestOutcomes.Success;
  expect.assertions(7);
  const expectedStatus = successExpected ? 200 : 404;
  const expectedStatusForDelete = successExpected ? 204 : 404;
  // after own user is deleted, the authorization is expected to become invalid
  const expectedStatusAfterDelete = userSubject === userObject ? 401 : 404;

  let response = await request.post('/api/login').send(credentials[userSubject]);
  const { accessToken } = response.body;
  expect(typeof accessToken).toBe('string');

  response = await request
    .get(`/api/users/${userIds[userObject]}`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatus);

  response = await request
    .patch(`/api/users/${userIds[userObject]}`)
    .send({
      firstName: 'Not so basic',
      lastName: 'After all',
    })
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatus);
  expect(response.body.firstName).toBe(successExpected ? 'Not so basic' : undefined);
  expect(response.body.lastName).toBe(successExpected ? 'After all' : undefined);

  response = await request
    .delete(`/api/users/${userIds[userObject]}`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatusForDelete);

  response = await request
    .get(`/api/users/${userIds[userObject]}`)
    .set('Authorization', `Bearer ${accessToken}`);

  expect(response.status).toBe(expectedStatusAfterDelete);
};

it('allows RUD on own profile to basic user', async () => {
  await expectRud('Basic', 'Basic', TestOutcomes.Success);
});

it('allows RUD on own profile to user manager', async () => {
  await expectRud('UserManager', 'UserManager', TestOutcomes.Success);
});

it('allows RUD on own profile to admin', async () => {
  await expectRud('Admin', 'Admin', TestOutcomes.Success);
});

it("denies RUD on user manager's profile to basic user", async () => {
  await expectRud('Basic', 'UserManager', TestOutcomes.Failure);
});

it("denies RUD on admins's profile to basic user", async () => {
  await expectRud('Basic', 'Admin', TestOutcomes.Failure);
});

it("allows RUD on basic user's profile to user manager", async () => {
  await expectRud('UserManager', 'Basic', TestOutcomes.Success);
});

it("allows RUD on admin's profile to user manager", async () => {
  await expectRud('UserManager', 'Admin', TestOutcomes.Success);
});

it("allows RUD on basic user's profile to user manager", async () => {
  await expectRud('Admin', 'Basic', TestOutcomes.Success);
});

it("allows RUD on admin's profile to user manager", async () => {
  await expectRud('Admin', 'UserManager', TestOutcomes.Success);
});
