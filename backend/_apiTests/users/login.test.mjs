/* eslint-disable  import/no-extraneous-dependencies */

import supertest from 'supertest';
import app from '../../app.mjs';

import { setupDB } from '../../setupTests.mjs';
import { generateRandomUser } from '../testData/testData.mjs';

setupDB();
const request = supertest(app);

it('logs user in with access and refresh tokens and user object', async () => {
  expect.assertions(3);
  const response = await request.post('/api/users/createAndLogIn').send(generateRandomUser());

  expect(response.body).toHaveProperty('accessToken');
  expect(response.body).toHaveProperty('refreshToken');
  expect(response.body).toHaveProperty('user');
});

it("doesn't log in with invalid credentials", async () => {
  const user = generateRandomUser();
  await request.post('/api/users').send(user);

  expect.assertions(6);

  const loginWith = (email, password) =>
    request.post('/api//login').send({
      email,
      password,
    });

  let response;

  response = await loginWith(`${user.email}x`, user.password);
  expect(response.status).toEqual(404);

  response = await loginWith(user.email, `${user.password}x`);
  expect(response.status).toEqual(404);

  response = await loginWith('', '');
  expect(response.status).toEqual(404);

  response = await loginWith(user.email, '');
  expect(response.status).toEqual(404);

  response = await loginWith(user.email, null);
  expect(response.status).toEqual(404);

  response = await loginWith(null, null);
  expect(response.status).toEqual(404);
});
