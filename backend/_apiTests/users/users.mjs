export const constructCreateAndLoginUser = (request) => async (user) => {
  await request.post('/api/users/createAndLogIn').send(user);

  const loginResponse = await request.post('/login').send({
    email: user.email,
    password: user.password,
  });

  return loginResponse.body;
};
