import express from 'express';
import bodyParser from 'body-parser';

import mongodb from 'mongodb';
import * as AuthorizationRouter from './authorization/routesConfig.mjs';
import * as UsersRouter from './users/routesConfig.mjs';
import * as TimeZonesRouter from './timeZone/routesConfig.mjs';
import { ErrorTypes, getError, MongoCodes } from './common/errors.mjs';

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header(
    'Access-Control-Allow-Headers',
    'Accept, Authorization, Content-Type, X-Requested-With, Range'
  );
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200);
  }
  return next();
});

app.use(bodyParser.json());
app.use((error, req, res, next) => {
  if (error instanceof SyntaxError) {
    return res.status(500).send(getError('Invalid JSON format for request'));
  }
  if (error) {
    return res.status(500).send(getError('Internal server error'));
  }

  return next();
});

AuthorizationRouter.routesConfig(app);
UsersRouter.routesConfig(app);
TimeZonesRouter.routesConfig(app);

app.use((error, req, res, next) => {
  if (!error) {
    return next();
  }

  /* eslint-disable-next-line no-console */
  console.trace(error);

  if (error instanceof mongodb.MongoError && error.code === MongoCodes.Duplicate) {
    return res.status(500).send(getError(ErrorTypes.DuplicateRecord));
  }

  return res.status(500).send(getError(ErrorTypes.GenericError));
});

export default app;
