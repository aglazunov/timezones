import jwt from 'jsonwebtoken';
import crypto from 'crypto';

import config from '../../common/config/env.config.mjs';
import User from '../../users/models/User.mjs';
import { clearUserFields } from '../../users/controllers/UserController.mjs';

export const login = async (req, res) => {
  try {
    const refreshId = req.body.userId + config.jwtSecret;
    const salt = crypto.randomBytes(16).toString('base64');
    const hash = crypto.createHmac('sha512', salt).update(refreshId).digest('base64');
    req.body.refreshKey = salt;
    const token = jwt.sign(req.body, config.jwtSecret, {
      expiresIn: config.jwtExpiration,
    });
    const b = Buffer.from(hash);
    const refreshToken = b.toString('base64');
    const user = clearUserFields((await User.findById(req.body.userId)).toJSON(), true);

    return res.status(200).send({ accessToken: token, refreshToken, user });
  } catch (err) {
    return res.status(500).send({ errors: err });
  }
};

export const refreshToken = (req, res) => {
  try {
    req.body = req.jwt;
    const token = jwt.sign(req.body, config.jwtSecret);
    return res.status(201).send({ id: token });
  } catch (err) {
    return res.status(500).send({ errors: err });
  }
};
