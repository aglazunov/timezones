import crypto from 'crypto';
import { ErrorTypes, FieldValidationTypes, getError } from '../../common/errors.mjs';
import User from '../../users/models/User.mjs';

// http://emailregex.com/
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const loginRequestValidator = (req, res, next) => {
  const fields = [];

  if (!req.body || !req.body.email) {
    fields.push({
      name: 'email',
      type: FieldValidationTypes.Missing,
    });
  }
  if (!req.body || !req.body.password) {
    fields.push({
      name: 'password',
      type: FieldValidationTypes.Missing,
    });
  }

  if (fields.length) {
    return res.status(400).send(
      getError(ErrorTypes.ValidationError, {
        fields,
      })
    );
  }

  return next();
};

export const credentialsChecker = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (user) {
    const passwordFields = user.password.split('$');
    const salt = passwordFields[0];
    const hash = crypto.createHmac('sha512', salt).update(req.body.password).digest('base64');

    if (hash === passwordFields[1]) {
      req.body = {
        userId: user._id,
      };

      return next();
    }
  }

  return res.status(400).send(getError('Invalid credentials'));
};

const isValidEmail = (email) => Boolean(EMAIL_REGEX.exec(email));

const isValidPassword = (password) =>
  // no password policy – just disallow empty
  password.length > 0;
export const createUserValidator = async (req, res, next) => {
  const fields = [];

  if (!req.body || !req.body.email) {
    fields.push({
      name: 'email',
      type: FieldValidationTypes.Missing,
    });
  }

  if (req.body.email && !isValidEmail(req.body.email)) {
    fields.push({
      name: 'email',
      type: FieldValidationTypes.Invalid,
    });
  }

  if (!req.body || !req.body.password) {
    fields.push({
      name: 'password',
      type: FieldValidationTypes.Missing,
    });
  }

  if (req.body.password && !isValidPassword(req.body.password)) {
    fields.push({
      name: 'password',
      type: FieldValidationTypes.Invalid,
    });
  }

  if (fields.length) {
    return res.status(400).send(
      getError(ErrorTypes.ValidationError, {
        fields,
      })
    );
  }

  return next();
};
