import * as VerifyUserMiddleware from './middlewares/verifyUserMiddleware.mjs';
import * as AuthorizationController from './controllers/AuthorizationController.mjs';
import asyncHandler from '../common/middlewares/asyncHandler.mjs';

export const routesConfig = (app) => {
  app.post(
    '/api/login',
    [
      VerifyUserMiddleware.loginRequestValidator,
      VerifyUserMiddleware.credentialsChecker,
      AuthorizationController.login,
    ].map(asyncHandler)
  );
  app.post('/api/refresh', [AuthorizationController.refreshToken].map(asyncHandler));
};
