const config = {
  port: 3600,
  appEndpoint: 'http://localhost:3600',
  apiEndpoint: 'http://localhost:3600',
  jwtSecret: 'bebEgM3Cx4jBM#DmEs7hUO3Z14EM^9Wh',
  jwtExpiration: 36000,
  environment: 'dev',
  dbUri: 'mongodb://localhost:27017/timezones',
};

export default config;
