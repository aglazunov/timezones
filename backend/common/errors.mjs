export const MongoCodes = {
  Duplicate: 11000,
};

export const ErrorTypes = {
  NoUserId: 'NoUserId',
  PermissionDenied: 'PermissionDenied',
  ValidationError: 'ValidationError',
  GenericError: 'GenericError',
  InvalidAccessToken: 'InvalidAccessToken',
  NoSuchUser: 'NoSuchUser',
  NotFound: 'NotFound',
  DuplicateRecord: 'DuplicateRecord',
};

export const FieldValidationTypes = {
  Missing: 'Missing',
  Invalid: 'Invalid',
};

export const getError = (arg1, arg2) => {
  let message;
  let type = arg1;
  const details = arg2;

  if (!ErrorTypes[arg1]) {
    message = arg1;
    // eslint-disable-next-line no-param-reassign
    type = ErrorTypes.GenericError;
  } else if (typeof arg2 === 'string') {
    message = arg2;
  }

  const error = { type };
  if (typeof message === 'string') {
    error.message = message;
  }
  if (details instanceof Object) {
    error.details = details;
  }

  return {
    status: 'error',
    error,
  };
};
