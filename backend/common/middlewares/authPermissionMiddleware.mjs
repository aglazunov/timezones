import { ErrorTypes, getError } from '../errors.mjs';
import { isPermitted, PermissionTypes } from '../config/permissions.mjs';

export const permissionExceeds = (a, b) => Boolean((a ^ b) & a);

export const permissionGate = (requiredPermission) => (req, res, next) => {
  const userPermissions = req.user.permissionLevel;

  if (userPermissions & requiredPermission) {
    return next();
  }

  return res.status(403).send(getError(ErrorTypes.PermissionDenied));
};

export const itemPermissionFilter = (fn) => (req, res, next) => {
  const userId = req.user.id;
  const userPermissions = req.user.permissionLevel;
  const permitted = (requiredPermission) => Boolean(requiredPermission & userPermissions);

  try {
    req.filter = fn({ userId, permitted });
  } catch (e) {
    if (typeof e === 'function') {
      return e(req, res, next);
    }
  }

  return next();
};

export const checkPermissionChange = (req, res, next) => {
  const newPermissions = parseInt(req.body.permissionLevel, 10);
  const currentUserPermissions = req.user.permissionLevel;

  // only do checks if permissionLevel change is requested

  if (Number.isNaN(newPermissions)) {
    delete req.body.permissionLevel;
    return next();
  }
  req.body.permissionLevel = newPermissions;

  const userId = req.user.id;
  const permitted = (requiredPermission) => Boolean(requiredPermission & currentUserPermissions);
  const ownUser = req.params.userId && userId === req.params.userId;

  // users can manage other's permissions if they have ManageOtherUsers
  // but they can't elevate them further than their own permissions

  const generallyPermitted =
    (ownUser && permitted(PermissionTypes.ChangeOwnPermissions)) ||
    (!ownUser && permitted(PermissionTypes.ChangeOtherPermissions));

  if (!generallyPermitted) {
    return res.status(403).send(getError('Permission change is not allowed'));
  }
  if (permissionExceeds(newPermissions, currentUserPermissions)) {
    // if own permission change is allowed, then only downward
    return res.status(403).send(getError('Permission elevation is not allowed'));
  }

  return next();
};

export const userRecordPermissionGate = (req, res, next) => {
  const userPermissions = req.user.permissionLevel;
  const userId = req.user.id;

  const permitted = (requiredPermission) => isPermitted(requiredPermission, userPermissions);
  const ownUser = req.params.userId && userId === req.params.userId;
  const permission = ownUser ? PermissionTypes.ManageOwnUser : PermissionTypes.ManageOtherUsers;

  if (permitted(permission)) {
    return next();
  }

  // if permission denied, just don't expose the fact that such a user exists
  return res.status(404).send(getError(ErrorTypes.NotFound));
};
