import jwt from 'jsonwebtoken';
import crypto from 'crypto';

import config from '../config/env.config.mjs';
import { ErrorTypes, FieldValidationTypes, getError } from '../errors.mjs';
import User from '../../users/models/User.mjs';
import { clearUserFields } from '../../users/controllers/UserController.mjs';
import { isPermitted, PermissionTypes } from '../config/permissions.mjs';

const AUTH_HEADER = 'authorization';
const AUTH_BEARER = 'Bearer';

export const verifyRefreshBodyField = (req, res, next) => {
  if (req.body && req.body.refreshToken) {
    return next();
  }

  const error = getError(ErrorTypes.ValidationError, {
    fields: [
      {
        name: 'refresh token',
        type: FieldValidationTypes.Missing,
      },
    ],
  });

  return res.status(400).send(error);
};

export const validRefreshNeeded = (req, res, next) => {
  const b = Buffer.from(req.body.refreshToken, 'base64');
  const refreshToken = b.toString();
  const hash = crypto
    .createHmac('sha512', req.jwt.refreshKey)
    .update(req.jwt.userId + config.jwtSecret)
    .digest('base64');

  if (hash === refreshToken) {
    req.body = req.jwt;

    return next();
  }

  const error = getError(ErrorTypes.ValidationError, {
    fields: [
      {
        name: 'refresh token',
        type: FieldValidationTypes.Invalid,
      },
    ],
  });

  return res.status(400).send(error);
};

export const authorized = async (req, res, next) => {
  const authHeader = req.headers[AUTH_HEADER];

  if (!authHeader) {
    return res.status(401).send(getError(ErrorTypes.InvalidAccessToken));
  }

  try {
    const authorization = authHeader.split(' ');

    if (authorization.length < 2 || authorization[0] !== AUTH_BEARER) {
      return res.status(401).send(getError(ErrorTypes.InvalidAccessToken));
    }

    req.jwt = jwt.verify(authorization[1], config.jwtSecret);
  } catch (err) {
    return res.status(401).send(getError(ErrorTypes.InvalidAccessToken));
  }

  const user = await User.findById(req.jwt.userId);

  if (!user) {
    return res.status(401).send(getError(ErrorTypes.InvalidAccessToken));
  }

  const userSerialization = clearUserFields(user.toJSON(), true);

  // update to current user values
  req.jwt = jwt;
  req.user = {
    ...userSerialization,
    id: userSerialization.id,
  };

  return next();
};

// user manager / admin can change user's passwords just like so
// for a regular user, confirmation with a valid currentPassword is required
export const checkValidPasswordChange = async (req, res, next) => {
  const { currentPassword, password } = req.body || {};

  if (password) {
    const requirePasswordConfirmation = !isPermitted(
      PermissionTypes.ManageOtherUsers,
      req.user.permissionLevel
    );

    if (!requirePasswordConfirmation) {
      return next();
    }

    if (!currentPassword) {
      return res.status(400).send(getError('Current password not specified for password change.'));
    }

    const user = await User.findOne({ email: req.body.email });

    if (user) {
      const passwordFields = user.password.split('$');
      const salt = passwordFields[0];
      const hash = crypto.createHmac('sha512', salt).update(currentPassword).digest('base64');

      if (hash === passwordFields[1]) {
        return next();
      }
      return res.status(400).send(getError("Current password doesn't match."));
    }

    return res.status(400).send(getError('User not found.'));
  }

  return next();
};
