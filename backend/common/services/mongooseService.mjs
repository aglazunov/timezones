import mongoose from 'mongoose';
import config from '../config/env.config.mjs';

mongoose.set('returnOriginal', false);

let count = 0;

export const mongooseOptions = {
  autoIndex: true,
  useCreateIndex: true,
  useFindAndModify: false,
  poolSize: 10,
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export const connectWithRetry = async () => {
  /* eslint-disable no-console */

  console.log('MongoDB connection with retry');

  try {
    await mongoose.connect(config.dbUri, mongooseOptions);
    console.log('MongoDB is connected');
  } catch (e) {
    console.log('MongoDB connection unsuccessful, retry after 5 seconds. ', ++count);
    setTimeout(connectWithRetry, 5000);
  }
};
