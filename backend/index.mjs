import config from './common/config/env.config.mjs';
import app from './app.mjs';
import { connectWithRetry } from './common/services/mongooseService.mjs';

app.listen(config.port, () => {
  /* eslint-disable-next-line no-console */
  console.log('app listening at port %s', config.port);

  connectWithRetry();
});
