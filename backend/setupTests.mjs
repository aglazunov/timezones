import mongoose from 'mongoose';
import { mongooseOptions } from './common/services/mongooseService.mjs';
import { testData } from './_apiTests/testData/testData.mjs';
import User from './users/models/User.mjs';
import TimeZone from './timeZone/models/TimeZone.mjs';

mongoose.set('useCreateIndex', true);
mongoose.set('returnOriginal', false);
mongoose.promise = global.Promise;

async function removeAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    await collection.deleteMany();
  }
}

async function dropAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    try {
      await collection.drop();
    } catch (error) {
      // Sometimes this error happens, but you can safely ignore it
      const safeToProceed =
        error.message === 'ns not found' ||
        error.message.includes('a background operation is currently running');
      if (safeToProceed) {
        return;
      }
      console.log(error.message);
    }
  }
}

const provideTestData = async () => {
  await User.collection.insertMany(testData.users);
  await TimeZone.collection.insertMany(testData.timeZones);
};

export const setupDB = (databaseName, withData) => {
  if (!databaseName) {
    databaseName = `test-${Math.floor(Math.random() * 10000)}`;
  }

  // Connect to Mongoose
  beforeAll(async () => {
    const url = `mongodb://localhost:27017/${databaseName}`;
    await mongoose.connect(url, mongooseOptions);
    await removeAllCollections();
  });

  beforeEach(async () => {
    await removeAllCollections();

    if (withData) {
      await provideTestData();
    }
  });

  // Cleans up database between each test
  afterEach(async () => {
    await removeAllCollections();
  });

  // Disconnect Mongoose
  afterAll(async () => {
    await dropAllCollections();
    await mongoose.connection.close();
  });
};
