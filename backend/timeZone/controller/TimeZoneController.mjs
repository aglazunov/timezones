import { ErrorTypes, FieldValidationTypes, getError } from '../../common/errors.mjs';
import { isPermitted, PermissionTypes } from '../../common/config/permissions.mjs';
import TimeZone from '../models/TimeZone.mjs';

const getApplicableUserId = (req) => {
  const userPermissions = req.user.permissionLevel;
  const ownPermitted = isPermitted(PermissionTypes.ManageOwnRecords, userPermissions);
  const othersPermitted = isPermitted(PermissionTypes.ManageOtherRecords, userPermissions);

  const applicableUserId = req.params.userId || req.user.id;
  const sameUser = req.user.id === applicableUserId;
  const hasPermission = sameUser ? ownPermitted : othersPermitted;

  if (!hasPermission) {
    return null;
  }

  return applicableUserId;
};

export const insert = async (req, res) => {
  const params = req.body;
  const userId = getApplicableUserId(req);

  if (!userId) {
    return res.status(403).send(getError(ErrorTypes.PermissionDenied));
  }

  const { name, city } = params;

  const timeZone = new TimeZone({
    name,
    city,
    compoundKey: [name, city, userId].join('$'),
    userId,
  });

  const result = await timeZone.save();
  return res.status(201).send(result.toJSON());
};

export const list = async (req, res) => {
  const userId = getApplicableUserId(req);

  if (!userId) {
    return res.status(403).send(getError(ErrorTypes.PermissionDenied));
  }

  try {
    const documents = await TimeZone.find({
      userId,
    }).exec();
    const result = documents.map((doc) => doc.toJSON());

    return res.status(200).send(result);
  } catch (e) {
    return res.status(500).send(getError('Failed getting time zone list'));
  }
};

export const getById = async (req, res) => {
  const id = req.params.timeZoneId;
  const timeZone = await TimeZone.findById(id);

  if (!timeZone) {
    return res.status(404).send(getError(ErrorTypes.NotFound));
  }

  return res.status(200).send(timeZone.toJSON());
};

// only city update is possible
export const patchById = async (req, res) => {
  const id = req.params.timeZoneId;
  const { city } = req.body;
  const userId = req.user.id;

  if (!city) {
    return res.status(404).send(getError(ErrorTypes.ValidationError), {
      fields: [{ name: 'city', type: FieldValidationTypes.Missing }],
      message: 'City is required.',
    });
  }

  let zone = await TimeZone.findById(id);

  if (!zone) {
    return res.status(404).send(getError(ErrorTypes.NotFound));
  }

  zone = await TimeZone.findByIdAndUpdate(id, {
    city,
    compoundKey: [zone.name, city, userId].join('$'),
  });

  return res.status(200).send(zone.toJSON());
};

export const removeById = async (req, res) => {
  const id = req.params.timeZoneId;
  await TimeZone.deleteOne({ _id: id });
  return res.status(204).send({});
};
