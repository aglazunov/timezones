import { isPermitted, PermissionTypes } from '../../common/config/permissions.mjs';
import { ErrorTypes, getError } from '../../common/errors.mjs';
import TimeZone from '../models/TimeZone.mjs';

// returns null if not permitted
const getApplicableUserId = (req) => {
  const userPermissions = req.user.permissionLevel;
  const ownPermitted = isPermitted(PermissionTypes.ManageOwnRecords, userPermissions);
  const othersPermitted = isPermitted(PermissionTypes.ManageOtherRecords, userPermissions);

  if (!ownPermitted && !othersPermitted) {
    return null;
  }

  const timeZone = TimeZone.findById(req.params.timeZoneId);
  if (!timeZone) {
    return false;
  }

  const applicableUserId = req.params.userId || req.user.id;
  const sameUser = req.user.id === applicableUserId;
  const hasPermission = sameUser ? ownPermitted : othersPermitted;

  if (!hasPermission) {
    return null;
  }

  return applicableUserId;
};

export const timeZoneRecordPermissionGate = (req, res, next) => {
  const timeZoneOwnerUserId = getApplicableUserId(req);

  if (timeZoneOwnerUserId) {
    return next();
  }

  // don't expose the fact that permission is denied
  return res.status(404).send(getError(ErrorTypes.NotFound));
};
