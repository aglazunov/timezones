import { ErrorTypes, FieldValidationTypes, getError } from '../../common/errors.mjs';

export const timeZoneValidator = (req, res, next) => {
  const requiredFields = ['name', 'city'];

  const errorFields = requiredFields
    .map((field) => {
      const value = req.body[field];

      if (!value) {
        return {
          name: 'name',
          type: FieldValidationTypes.Missing,
        };
      }

      if (typeof value !== 'string') {
        return {
          name: 'name',
          type: FieldValidationTypes.Invalid,
        };
      }

      return false;
    })
    .filter(Boolean);

  if (errorFields.length) {
    return res.status(400).send(
      getError(ErrorTypes.ValidationError, {
        fields: errorFields,
      })
    );
  }

  return next();
};
