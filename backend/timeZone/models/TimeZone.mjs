import mongoose from 'mongoose';

const timeZoneSchema = new mongoose.Schema({
  compoundKey: { type: String, required: true, unique: true, dropDups: true },
  name: { type: String, required: true },
  city: { type: String, required: true },
  userId: { type: String, required: true },
});

const TimeZone = mongoose.model('TimeZone', timeZoneSchema);

export default TimeZone;
