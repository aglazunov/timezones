import * as TimeZoneController from './controller/TimeZoneController.mjs';
import * as ValidationMiddleware from '../common/middlewares/authValidationMiddleware.mjs';
import * as PermissionMiddleware from './middlewares/timeZonePermissionMiddleware.mjs';
import * as VerifyTimeZoneMiddleware from './middlewares/verifyTimeZoneMiddleware.mjs';
import asyncHandler from '../common/middlewares/asyncHandler.mjs';

export const routesConfig = (app) => {
  // add timezone for current user
  app.post(
    '/api/timeZones',
    [
      ValidationMiddleware.authorized,
      VerifyTimeZoneMiddleware.timeZoneValidator,
      TimeZoneController.insert,
    ].map(asyncHandler)
  );
  // add timezone for a specified user
  app.post(
    '/api/users/:userId/timeZones',
    [
      ValidationMiddleware.authorized,
      VerifyTimeZoneMiddleware.timeZoneValidator,
      TimeZoneController.insert,
    ].map(asyncHandler)
  );
  // lists timezone of a certain user
  app.get(
    '/api/users/:userId/timeZones',
    [ValidationMiddleware.authorized, TimeZoneController.list].map(asyncHandler)
  );
  // lists timezones of current user
  app.get(
    '/api/timeZones',
    [ValidationMiddleware.authorized, TimeZoneController.list].map(asyncHandler)
  );
  app.get(
    '/api/timeZones/:timeZoneId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.timeZoneRecordPermissionGate,
      TimeZoneController.getById,
    ].map(asyncHandler)
  );
  app.patch(
    '/api/timeZones/:timeZoneId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.timeZoneRecordPermissionGate,
      TimeZoneController.patchById,
    ].map(asyncHandler)
  );
  app.delete(
    '/api/timeZones/:timeZoneId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.timeZoneRecordPermissionGate,
      TimeZoneController.removeById,
    ].map(asyncHandler)
  );
};
