import crypto from 'crypto';
import mongodb from 'mongodb';

import { isPermitted, PermissionTypes, Roles } from '../../common/config/permissions.mjs';
import { ErrorTypes, getError, MongoCodes } from '../../common/errors.mjs';
import User from '../models/User.mjs';
import TimeZone from '../../timeZone/models/TimeZone.mjs';

export const clearUserFields = (user, leavePermissionInformation) => {
  const { password, permissionLevel, ...userData } = user;

  if (leavePermissionInformation) {
    userData.permissionLevel = permissionLevel;
  }

  return userData;
};

export const hashAndSaltPassword = (password) => {
  const salt = crypto.randomBytes(16).toString('base64');
  const hash = crypto.createHmac('sha512', salt).update(password).digest('base64');

  return [salt, hash].join('$');
};

// backdoor to getting manager or admin rights
const getNewUserPermissionLevel = (user) => {
  if (user.email === 'manager@example.com') {
    return Roles.UserManager;
  }
  if (user.email === 'admin@example.com') {
    return Roles.Admin;
  }

  return Roles.Basic;
};

const insertBase = async (req, res) => {
  const params = {
    ...req.body,
  };

  params.password = hashAndSaltPassword(params.password);
  params.permissionLevel = getNewUserPermissionLevel(params);

  const user = new User(params);
  try {
    await user.save();

    return user;
  } catch (error) {
    if (error instanceof mongodb.MongoError && error.code === MongoCodes.Duplicate) {
      return res
        .status(500)
        .send(getError(ErrorTypes.DuplicateRecord, 'User with this email is already registered.'));
    }
    throw error;
  }
};

export const insert = async (req, res, next) => {
  const user = await insertBase(req, res, next);

  return res.status(201).send(user.toJSON());
};

export const insertWithSubsequentOperation = async (req, res, next) => {
  await insertBase(req, res, next);
  next();
};

export const list = async (req, res) => {
  const userPermissions = req.user.permissionLevel;
  const ownPermitted = isPermitted(PermissionTypes.ManageOwnUser, userPermissions);
  const othersPermitted = isPermitted(PermissionTypes.ManageOtherUsers, userPermissions);

  if (!ownPermitted && !othersPermitted) {
    return res.status(403).send(getError(ErrorTypes.PermissionDenied));
  }

  let filter;

  if (!ownPermitted || !othersPermitted) {
    const conditionOperator = ownPermitted ? '$eq' : '$ne';

    filter = {
      _id: {
        [conditionOperator]: req.user.id,
      },
    };
  }

  const users = await User.find(filter);
  const result = users.map((user) => clearUserFields(user.toJSON(), true));
  return res.status(200).send(result);
};

export const getById = async (req, res) => {
  const user = await User.findById(req.params.userId);
  if (!user) {
    return res.status(404).send(getError(ErrorTypes.NotFound));
  }

  const result = clearUserFields(user.toJSON(), true);

  return res.status(200).send(result);
};

const pickDefinedFields = (fields, object) =>
  fields.reduce((acc, field) => {
    if (typeof object[field] !== 'undefined') {
      acc[field] = object[field];
    }
    return acc;
  }, {});

export const patchById = async (req, res) => {
  const params = pickDefinedFields(
    ['firstName', 'lastName', 'email', 'permissionLevel', 'password'],
    req.body
  );

  if (params.password) {
    params.password = hashAndSaltPassword(params.password);
  }

  let user;

  try {
    user = await User.findByIdAndUpdate(req.params.userId, params);
  } catch (error) {
    if (error instanceof mongodb.MongoError && error.code === MongoCodes.Duplicate) {
      return res
        .status(500)
        .send(getError(ErrorTypes.DuplicateRecord, 'User with this email is already registered.'));
    }

    throw error;
  }

  if (!user) {
    return res.status(404).send(getError(ErrorTypes.NotFound));
  }

  return res.status(200).send(clearUserFields(user.toJSON(), true));
};

export const removeById = async (req, res) => {
  await User.deleteOne({ _id: req.params.userId });
  await TimeZone.deleteMany({
    userId: req.params.userId,
  });

  return res.status(204).send({});
};
