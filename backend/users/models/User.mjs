import mongoose from 'mongoose';
import TimeZone from '../../timeZone/models/TimeZone.mjs';

const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: { type: String, unique: true },
  password: { type: String, minlength: 1 },
  permissionLevel: { type: Number, required: true },
});

userSchema.virtual('id').get(function getId() {
  return this._id.toHexString();
});

// Ensure virtual fields are serialised.
userSchema.set('toJSON', { virtuals: true });

userSchema.findById = function findById() {
  return this.model('Users').find({ id: this.id });
};

userSchema.pre('remove', async function deleteLinkedTimeZones(next) {
  await TimeZone.deleteMany({
    userId: this._id.toHexString(),
  });

  return next();
});

export default mongoose.model('User', userSchema);
