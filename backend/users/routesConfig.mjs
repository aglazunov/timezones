import * as UserController from './controllers/UserController.mjs';
import * as ValidationMiddleware from '../common/middlewares/authValidationMiddleware.mjs';
import * as PermissionMiddleware from '../common/middlewares/authPermissionMiddleware.mjs';
import * as VerifyUserMiddleware from '../authorization/middlewares/verifyUserMiddleware.mjs';
import asyncHandler from '../common/middlewares/asyncHandler.mjs';
import * as AuthorizationController from '../authorization/controllers/AuthorizationController.mjs';

export const routesConfig = (app) => {
  app.post(
    '/api/users',
    [VerifyUserMiddleware.createUserValidator, UserController.insert].map(asyncHandler)
  );
  app.post(
    '/api/users/createAndLogIn',
    [
      VerifyUserMiddleware.createUserValidator,
      UserController.insertWithSubsequentOperation,
      VerifyUserMiddleware.loginRequestValidator,
      VerifyUserMiddleware.credentialsChecker,
      AuthorizationController.login,
    ].map(asyncHandler)
  );
  // permission based filter
  // if the user can manage other users
  app.get('/api/users', [ValidationMiddleware.authorized, UserController.list].map(asyncHandler));
  app.get(
    '/api/users/:userId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.userRecordPermissionGate,
      UserController.getById,
    ].map(asyncHandler)
  );
  app.patch(
    '/api/users/:userId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.userRecordPermissionGate,
      PermissionMiddleware.checkPermissionChange,
      ValidationMiddleware.checkValidPasswordChange,
      UserController.patchById,
    ].map(asyncHandler)
  );
  app.delete(
    '/api/users/:userId',
    [
      ValidationMiddleware.authorized,
      PermissionMiddleware.userRecordPermissionGate,
      UserController.removeById,
    ].map(asyncHandler)
  );
};
