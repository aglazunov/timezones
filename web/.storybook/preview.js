import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import 'semantic-ui-less/semantic.less';
import 'typeface-roboto';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

export const decorators = [(Story) => <MemoryRouter>{<Story />}</MemoryRouter>];
