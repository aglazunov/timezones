const generateRandomEmail = () => `test-${Math.floor(Math.random() * 10000)}@example.com`;
const email = generateRandomEmail();
const password = 'testP@$$';

describe('Sign up', () => {
  it('Signs user up and logs in', () => {
    cy.visit('/');

    cy.contains('Log In');
    cy.get('[data-cy=signup-link]').click();
    cy.contains('Sign Up');

    cy.get('[data-cy=firstName]').type('Nondescript');
    cy.get('[data-cy=lastName]').type('User');
    cy.get('[data-cy=email]').type(email);
    cy.get('[data-cy=password]').type(password);
    cy.get('[data-cy=confirmPassword]').type('testP@$$');

    cy.get('[data-cy=submitButton]').click();
  });

  it('Adds new time zones', () => {
    cy.contains('Add Time Zone');

    cy.get('[data-cy=addTimeZone]').click();
    cy.get('[data-cy=searchField]').type('sey');
    cy.contains('Europe/Jersey');
    cy.contains('(SC)');
    cy.contains('Antarctica/Casey');
    cy.contains('Seychelles Time').click();

    cy.get('[data-cy=city]').should('have.value', 'Victoria').clear().type('Nike');
    cy.get('[data-cy=submitButton]').click();

    cy.get('[data-cy=addTimeZone]').click();
    cy.get('[data-cy=searchField]').clear().type('lisb');
    cy.contains('Europe/Lisbon').click();
    cy.contains('Porto').click();
    cy.get('[data-cy=city]').should('have.value', 'Porto');
    cy.get('[data-cy=submitButton]').click();

    cy.get('[data-cy=timeZoneList]').contains('WET');
  });

  it('Removes time zones', () => {
    cy.get('[data-cy=timeZoneList]').contains('Porto').click();
    cy.get('[data-cy=deleteButton]').click();
    cy.get('[data-cy=confirmDialog]').contains('Delete').click();

    cy.get('[data-cy=timeZoneList]').should('not.contain', 'WET');
    cy.get('[data-cy=timeZoneList]').should('not.contain', 'Porto');
  });

  it('Logs out', () => {
    cy.get('[data-cy=navProfile]').click();
    cy.get('[data-cy=signOutButton]').click();
    cy.contains('Log In');
  });

  it('Logs in and deletes a user, then redirects to login', () => {
    cy.get('[data-cy=email]').type(email);
    cy.get('[data-cy=password]').type(password);
    cy.get('[data-cy=submitButton]').click();

    cy.get('[data-cy=timeZoneList]').contains('Nike');

    cy.get('[data-cy=navProfile]').click();
    cy.get('[data-cy=deleteButton]').click();
    cy.get('[data-cy=confirmDialog]').contains('Delete').click();
    cy.contains('Log In');
  });
});
