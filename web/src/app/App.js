import React from 'react';

import './App.css';
import AuthHelper from './AuthHelper';
import AppRoutes from './routes/AppRoutes';

const App = () => (
  <AuthHelper>
    <div className="App">
      <AppRoutes />
    </div>
  </AuthHelper>
);

export default App;
