/* eslint no-console: 0 */

import React from 'react';
import { render, screen } from '@testing-library/react';

import App from './App';

test('renders text on the screen', () => {
  render(<App />);
  const element = screen.getByText(/Log In/i);
  expect(element).toBeInTheDocument();
});
