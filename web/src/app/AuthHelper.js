import React, { useLayoutEffect, useMemo, useState } from 'react';
import axios from 'axios';
import T from 'prop-types';

import AuthContext from './AuthContext';
import { ErrorTypes } from '../integration/errors';

let initialAuth;

try {
  initialAuth = JSON.parse(localStorage.getItem('auth'));
} catch (e) {
  // pass
}

const AuthHelper = ({ children }) => {
  const [authValue, setAuthValue] = useState(initialAuth);

  useLayoutEffect(() => {
    const requestInterceptorId = axios.interceptors.request.use((requestConfig) => {
      if (authValue instanceof Object) {
        // eslint-disable-next-line no-param-reassign
        requestConfig.headers.authorization = `Bearer ${authValue.accessToken}`;
      }

      return requestConfig;
    });

    const responseInterceptorId = axios.interceptors.response.use(
      (response) => {
        if (response.data instanceof Object && response.data.accessToken) {
          const value = {
            accessToken: response.data.accessToken,
            refreshToken: response.data.refreshToken,
            user: response.data.user,
          };
          setAuthValue(value);

          localStorage.setItem('auth', JSON.stringify(value));
        }

        return response;
      },
      (error) => {
        if (error.response?.data instanceof Object) {
          if (error.response.data.error?.type === ErrorTypes.InvalidAccessToken) {
            setAuthValue(null);
            localStorage.removeItem('auth');
          }
        }

        throw error;
      }
    );

    return () => {
      axios.interceptors.request.eject(requestInterceptorId);
      axios.interceptors.response.eject(responseInterceptorId);
    };
  }, [authValue]);

  const contextValue = useMemo(
    () => ({
      ...authValue,
      signOut: () => {
        setAuthValue(null);
        localStorage.removeItem('auth');
      },
      isAuthenticated: authValue instanceof Object,
    }),
    [authValue]
  );

  return <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>;
};

AuthHelper.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default AuthHelper;
