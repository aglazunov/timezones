import { BrowserRouter } from 'react-router-dom';
import React, { useContext } from 'react';

import NotAuthedRoutes from './NotAuthedRoutes';
import AuthedRoutes from './AuthedRoutes';
import AuthContext from '../AuthContext';
import FrameWrapper from '../../components/FrameWrapper';

const AppRoutes = () => {
  const { isAuthenticated } = useContext(AuthContext);

  return (
    <BrowserRouter>
      <FrameWrapper>{isAuthenticated ? <AuthedRoutes /> : <NotAuthedRoutes />}</FrameWrapper>
    </BrowserRouter>
  );
};

export default AppRoutes;
