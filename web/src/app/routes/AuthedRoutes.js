import { Redirect, Route, Switch } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';
import FormSizeWrapper from '../../form/components/FormSizeWrapper';
import ProfileForm from '../../auth/form/profile/ProfileForm';
import TimeZonesScreen from '../../timezones/list/TimeZonesScreen';
import NavFooter from '../../components/NavFooter';
import AddTimeZoneWorkflow from '../../timezones/add/AddTimeZoneWorkflow';
import EditTimeZoneForm from '../../timezones/form/editTimeZone/EditTimeZoneForm';
import UserManagement from '../../users/management/UserManagement';
import Box from '../../components/layout/Box';

const Backdrop = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.6);
  z-index: -1;
`;

const AuthedRoutes = () => (
  <>
    <Backdrop />
    <Box flexGrow={1}>
      <Switch>
        <Route exact path="/time-zones/:timeZoneId">
          <FormSizeWrapper>
            <EditTimeZoneForm />
          </FormSizeWrapper>
        </Route>
        <Route exact path="/users/:userId/time-zones">
          <TimeZonesScreen />
        </Route>
        <Route exact path="/users/:userId/profile">
          <FormSizeWrapper>
            <ProfileForm />
          </FormSizeWrapper>
        </Route>
        <Route exact path="/profile">
          <FormSizeWrapper>
            <ProfileForm />
          </FormSizeWrapper>
        </Route>
        <Route exact path="/users">
          <UserManagement />
        </Route>
        <Route exact path="/add">
          <AddTimeZoneWorkflow />
        </Route>
        <Route exact path="/users/:userId/time-zones/add">
          <AddTimeZoneWorkflow />
        </Route>
        <Route exact path="/">
          <TimeZonesScreen />
        </Route>
        <Redirect to="/" />
      </Switch>
    </Box>
    <NavFooter />
  </>
);

export default AuthedRoutes;
