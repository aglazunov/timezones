import { Redirect, Route, Switch } from 'react-router-dom';
import React from 'react';
import FormSizeWrapper from '../../form/components/FormSizeWrapper';
import LoginForm from '../../auth/form/login/LoginForm';
import SignupForm from '../../auth/form/signup/SignupForm';

const NotAuthedRoutes = () => (
  <Switch>
    <Route path="/log-in">
      <FormSizeWrapper>
        <LoginForm />
      </FormSizeWrapper>
    </Route>
    <Route path="/sign-up">
      <FormSizeWrapper>
        <SignupForm />
      </FormSizeWrapper>
    </Route>
    <Redirect to="/log-in" />
  </Switch>
);

export default NotAuthedRoutes;
