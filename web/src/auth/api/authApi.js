import axios from 'axios';
import { API_ENDPOINT } from '../../config';

export const logIn = (email, password) =>
  axios.post(`${API_ENDPOINT}/login`, {
    email,
    password,
  });

export const signUp = (email, password, userData) =>
  axios.post(`${API_ENDPOINT}/users/createAndLogIn`, {
    email,
    password,
    ...userData,
  });
