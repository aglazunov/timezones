import React from 'react';
import SubmitButton from './SubmitButton';

const ActionButtons = () => <SubmitButton data-cy="submitButton">Log In</SubmitButton>;

export default ActionButtons;
