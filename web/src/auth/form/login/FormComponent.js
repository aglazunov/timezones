import React, { useContext } from 'react';
import T from 'prop-types';

import { Card, Form } from 'semantic-ui-react';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import { statusChecks } from '../../../util/statuses';
import useDataLoading from '../../../hooks/useDataLoading';
import { logIn } from '../../api/authApi';

const SUBMIT_ACTION_TYPE = 'SUBMIT';

const FormComponent = ({ children }) => {
  const { handleSubmit, dispatch, state } = useContext(FormContext);
  const formInProgress = statusChecks.isInProgress(state.loadingState.status);
  const formError = statusChecks.isError(state.loadingState.status);

  const loginFn = ({ email, password }) => logIn(email, password);
  const fn = useDataLoading(SUBMIT_ACTION_TYPE, loginFn, dispatch);

  return (
    <Card fluid>
      <Card.Content>
        <Box p={3}>
          <Form onSubmit={handleSubmit(fn)} loading={formInProgress} error={formError}>
            {children}
          </Form>
        </Box>
      </Card.Content>
    </Card>
  );
};

FormComponent.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default FormComponent;
