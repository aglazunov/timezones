import React, { useReducer } from 'react';
import { useForm } from 'react-hook-form';

import { Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import FormContext from '../../../form/FormContext';
import { statusChecks } from '../../../util/statuses';
import FormComponent from './FormComponent';
import EmailField from './fields/EmailField';
import PasswordField from './fields/PasswordField';
import Flex from '../../../components/layout/Flex';
import Brand from '../../../components/Brand';
import Box from '../../../components/layout/Box';
import reducer, { initialState } from './redux/reducer';
import ActionButtons from './ActionButtons';

const defaultValues = {};

const LoginForm = () => {
  const { register, handleSubmit, errors } = useForm({
    defaultValues,
  });

  const [state, dispatch] = useReducer(reducer, initialState);
  const { loadingState } = state;

  const serverError = statusChecks.isError(loadingState.status) && loadingState.message;

  const contextValue = {
    register,
    errors,
    state,
    dispatch,
    handleSubmit,
  };

  return (
    <FormContext.Provider value={contextValue}>
      <FormComponent>
        <Box>
          <Flex justifyContent="center" mb={4}>
            <Brand />
          </Flex>
          <Message error content={serverError} />
        </Box>

        <Box mt={4} mb={3}>
          <EmailField />
          <PasswordField />
        </Box>

        <Box mb={3}>
          <ActionButtons />
        </Box>

        <Flex justifyContent="center">
          <span>Don&apos;t have an account yet?</span>
          &nbsp;
          <Link to="/sign-up" data-cy="signup-link">
            Sign up
          </Link>
        </Flex>
      </FormComponent>
    </FormContext.Provider>
  );
};

export default LoginForm;
