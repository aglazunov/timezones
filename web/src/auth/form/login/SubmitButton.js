import React, { useContext } from 'react';
import T from 'prop-types';

import FormContext from '../../../form/FormContext';
import { statusChecks } from '../../../util/statuses';

const SubmitButton = ({ children, ...props }) => {
  const { state } = useContext(FormContext);
  const { loadingState } = state;
  const submitting = statusChecks.isInProgress(loadingState);

  return (
    <input
      type="submit"
      className="ui button primary fluid large"
      disabled={submitting}
      value={children}
      {...props}
    />
  );
};

SubmitButton.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default SubmitButton;
