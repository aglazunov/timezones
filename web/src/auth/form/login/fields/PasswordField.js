import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

const PASSWORD_MIN_LENGTH = 1;

const fieldProps = {
  name: Fields.Password,
  label: 'Password',
  type: 'password',
  registerProps: {
    required: 'Password is required',
    minLength: {
      value: PASSWORD_MIN_LENGTH,
      message: `Password must be at least ${PASSWORD_MIN_LENGTH}-char long`,
    },
  },
  'data-cy': 'password',
};

const PasswordField = () => <FormField {...fieldProps} />;

export default PasswordField;
