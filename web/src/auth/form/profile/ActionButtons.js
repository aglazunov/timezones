import { Button } from 'semantic-ui-react';
import React, { useContext, useMemo } from 'react';
import { useHistory } from 'react-router-dom';

import Box from '../../../components/layout/Box';
import SubmitButton from '../login/SubmitButton';
import Flex from '../../../components/layout/Flex';
import AuthContext from '../../../app/AuthContext';
import useConfirm from '../../../form/useConfirm';
import FormContext from '../../../form/FormContext';
import useDataLoading from '../../../hooks/useDataLoading';
import { DELETE_ACTION_TYPE } from '../../../timezones/form/editTimeZone/redux/actionTypes';
import * as api from '../../../users/api/userApi';

const ActionButtons = () => {
  const { signOut } = useContext(AuthContext);
  const confirmDialog = useConfirm();
  const { formState, ownUser, dispatch, userId } = useContext(FormContext);
  const history = useHistory();

  const deleteUser = useMemo(() => () => api.deleteUser(userId), [userId]);
  const deleteFn = useDataLoading(DELETE_ACTION_TYPE, deleteUser, dispatch);

  const handleSignOutClick = (event) => {
    event.preventDefault();
    signOut();
  };

  const handleDeleteAccount = async (event) => {
    event.preventDefault();

    const confirmed = await confirmDialog({
      content: 'Are you sure to delete user account and all its time zones?',
      cancelButton: 'Cancel',
      confirmButton: 'Delete',
    });

    if (confirmed) {
      await deleteFn();
      history.goBack();
    }
  };

  const handleClose = async (event) => {
    event.preventDefault();
    let confirmed = true;

    if (formState.isDirty) {
      confirmed = await confirmDialog({
        content: 'You have unsaved changes to profile. Sure to discard them?',
        cancelButton: 'No',
        confirmButton: 'Yes',
      });
    }

    if (confirmed) {
      history.goBack();
    }
  };

  return (
    <Flex flexDirection="column">
      <Box mb={1}>
        <Button fluid onClick={handleDeleteAccount} color="red" data-cy="deleteButton">
          Delete Account
        </Button>
      </Box>
      {ownUser && (
        <Box mb={4}>
          <Button onClick={handleSignOutClick} fluid basic color="red" data-cy="signOutButton">
            Sign Out
          </Button>
        </Box>
      )}

      {formState.isDirty && (
        <Box mb={1}>
          <SubmitButton>Save</SubmitButton>
        </Box>
      )}
      <Box mb={3} basic secondary>
        <Button fluid onClick={handleClose}>
          Close
        </Button>
      </Box>
    </Flex>
  );
};

export default ActionButtons;
