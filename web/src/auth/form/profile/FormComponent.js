import React, { useContext, useMemo } from 'react';
import T from 'prop-types';

import { Card, Form } from 'semantic-ui-react';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import useDataLoading from '../../../hooks/useDataLoading';
import { SUBMIT_ACTION_TYPE } from './redux/actionTypes';
import { updateUser } from '../../../users/api/userApi';
import { isError, isInProgress } from './redux/selectors';

const FormComponent = ({ children }) => {
  const { state, handleSubmit, dispatch, userId } = useContext(FormContext);

  const updateFn = useMemo(
    () => ({ confirmNewPassword, ...values }) => updateUser(userId, values),
    [userId]
  );
  const fn = useDataLoading(SUBMIT_ACTION_TYPE, updateFn, dispatch);

  return (
    <Card fluid>
      <Card.Content>
        <Box p={3}>
          <Form onSubmit={handleSubmit(fn)} loading={isInProgress(state)} error={isError(state)}>
            {children}
          </Form>
        </Box>
      </Card.Content>
    </Card>
  );
};

FormComponent.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default FormComponent;
