import React, { useContext, useEffect, useMemo, useReducer, useState } from 'react';
import { useForm } from 'react-hook-form';
import T from 'prop-types';
import { useParams } from 'react-router-dom';

import { Checkbox, Message } from 'semantic-ui-react';
import FirstNameField from './fields/FirstNameField';
import LastNameField from './fields/LastNameField';
import EmailField from './fields/EmailField';
import FormContext from '../../../form/FormContext';
import FormComponent from './FormComponent';
import Brand from '../../../components/Brand';
import Flex from '../../../components/layout/Flex';
import Box from '../../../components/layout/Box';
import AuthContext from '../../../app/AuthContext';
import { loadUserProfile } from '../../../users/api/userApi';
import CurrentPasswordField from './fields/CurrentPasswordField';
import ConfirmPasswordField from './fields/ConfirmNewPasswordField';
import NewPasswordField from './fields/NewPasswordField';
import useDataLoading from '../../../hooks/useDataLoading';
import reducer, { initialState } from './redux/reducer';
import { LOAD_PROFILE_ACTION_TYPE } from './redux/actionTypes';
import FormConfirm from '../../../form/FormConfirm';
import ActionButtons from './ActionButtons';
import { getErrorMessage } from './redux/selectors';
import { isPermitted, PermissionTypes } from '../../../integration/permissions';
import RoleField from './fields/RoleField';

const ProfileForm = ({ onSubmit }) => {
  const {
    register,
    handleSubmit,
    errors,
    watch,
    reset,
    formState,
    getValues,
    setValue,
  } = useForm();

  const { user } = useContext(AuthContext);
  const password = watch('password');
  const [state, dispatch] = useReducer(reducer, initialState);
  const { profile } = state;
  const [changePassword, setChangePassword] = useState(false);
  const params = useParams();
  const userId = params.userId || user.id;
  const ownUser = userId === user.id;

  const serverError = getErrorMessage(state);

  const handleChangePasswordChange = (event, { checked }) => {
    setChangePassword(checked);
  };

  const getUserData = useMemo(() => () => loadUserProfile(userId), [userId]);
  const fetchData = useDataLoading(LOAD_PROFILE_ACTION_TYPE, getUserData, dispatch);
  useEffect(() => fetchData(), [fetchData]);

  useEffect(() => {
    profile && reset(profile);
    setChangePassword(false);
  }, [reset, profile]);

  const managePermitted = isPermitted(PermissionTypes.ManageOtherUsers, user.permissionLevel);

  const contextValue = {
    userId,
    ownUser,
    register,
    errors,
    state,
    dispatch,
    handleSubmit,
    onSubmit,
    password,
    formState,
    getValues,
    setValue,
    watch,
  };

  return (
    <FormContext.Provider value={contextValue}>
      <FormComponent>
        <Box>
          <Flex justifyContent="center" mb={4}>
            <Brand />
          </Flex>
          <Message error content={serverError} />
        </Box>

        <Box mt={4} mb={3}>
          <FirstNameField />
          <LastNameField />
          <EmailField />
          <RoleField />
          <Box mb={3}>
            <Checkbox
              label="Change password"
              onChange={handleChangePasswordChange}
              checked={changePassword}
            />
          </Box>
          {changePassword && (
            <>
              {!managePermitted && <CurrentPasswordField />}
              <NewPasswordField />
              <ConfirmPasswordField />
            </>
          )}
        </Box>

        <ActionButtons />
        <FormConfirm />
      </FormComponent>
    </FormContext.Provider>
  );
};

ProfileForm.propTypes = {
  onSubmit: T.func,
};

ProfileForm.defaultProps = {
  onSubmit: null,
};

export default ProfileForm;
