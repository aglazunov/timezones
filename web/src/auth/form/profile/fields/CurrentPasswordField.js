import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

const PASSWORD_MIN_LENGTH = 1;

const fieldProps = {
  name: Fields.CurrentPassword,
  label: 'Current Password',
  type: 'password',
  registerProps: {
    required: 'Current Password is required',
    minLength: {
      value: PASSWORD_MIN_LENGTH,
      message: `Password must be at least ${PASSWORD_MIN_LENGTH}-char long`,
    },
  },
};

const CurrentPasswordField = () => <FormField {...fieldProps} />;

export default CurrentPasswordField;
