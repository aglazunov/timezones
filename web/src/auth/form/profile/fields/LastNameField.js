import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

const fieldProps = {
  name: Fields.LastName,
  label: 'Last Name',
  type: 'text',
  registerProps: {
    required: 'Last name is required',
  },
};

const LastNameField = () => <FormField {...fieldProps} />;

export default LastNameField;
