import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

const PASSWORD_MIN_LENGTH = 1;

const fieldProps = {
  name: Fields.NewPassword,
  label: 'New Password',
  type: 'password',
  registerProps: {
    required: 'New password is required',
    minLength: {
      value: PASSWORD_MIN_LENGTH,
      message: `New password must be at least ${PASSWORD_MIN_LENGTH}-char long`,
    },
  },
};

const NewPasswordField = () => <FormField {...fieldProps} />;

export default NewPasswordField;
