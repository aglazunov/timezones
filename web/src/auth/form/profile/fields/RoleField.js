import React, { useContext, useMemo } from 'react';
import { Dropdown } from 'semantic-ui-react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';
import AuthContext from '../../../../app/AuthContext';
import FormContext from '../../../../form/FormContext';
import { isPermitted, PermissionTypes, Roles } from '../../../../integration/permissions';
import Box from '../../../../components/layout/Box';

const fieldProps = {
  name: Fields.PermissionLevel,
  label: 'Role',
  type: 'text',
  registerProps: {},
  hidden: true,
};

const standardRoleOptions = Object.entries(Roles).map(([key, permissions]) => ({
  key,
  value: String(permissions),
  text: key,
}));

const RoleLabelMap = Object.entries(Roles).reduce((acc, [key, permissions]) => {
  acc[permissions] = key;

  return acc;
}, {});

const RoleField = () => {
  const { user } = useContext(AuthContext);
  const ownPermissionLevel = user.permissionLevel;

  const { ownUser, setValue, watch } = useContext(FormContext);
  const canChange =
    (ownUser && isPermitted(PermissionTypes.ChangeOwnPermissions, ownPermissionLevel)) ||
    (!ownUser && isPermitted(PermissionTypes.ChangeOtherPermissions, ownPermissionLevel));

  const rawPermissionLevel = watch('permissionLevel');
  const permissionLevelSpecified =
    (typeof rawPermissionLevel === 'number' || typeof rawPermissionLevel === 'string') &&
    rawPermissionLevel !== '';
  const permissionLevel = permissionLevelSpecified ? String(rawPermissionLevel) : null;

  const options = useMemo(() => {
    // unset or standard permission
    if (permissionLevel === null || RoleLabelMap[permissionLevel]) {
      return standardRoleOptions;
    }

    return [
      ...standardRoleOptions,
      {
        text: `Custom permissions (${permissionLevel})`,
        value: permissionLevel,
        key: permissionLevel,
      },
    ];
  }, [permissionLevel]);

  const handleChange = (event, { value }) => {
    setValue('permissionLevel', value, {
      shouldDirty: true,
    });
  };

  return (
    <>
      <FormField {...fieldProps} />
      <Box mb={2}>
        {canChange && (
          <Dropdown
            selection
            placeholder="Select Role"
            fluid
            options={options}
            value={permissionLevel}
            onChange={handleChange}
          />
        )}
        {!canChange &&
          String(RoleLabelMap[permissionLevel] || `Custom permissions (${permissionLevel})`)}
      </Box>
    </>
  );
};

export default RoleField;
