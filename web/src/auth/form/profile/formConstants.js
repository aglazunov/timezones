export const Fields = {
  FirstName: 'firstName',
  LastName: 'lastName',
  Email: 'email',
  CurrentPassword: 'currentPassword',
  NewPassword: 'password',
  ConfirmNewPassword: 'confirmNewPassword',
  PermissionLevel: 'permissionLevel',
};
