import { combineReducers } from 'redux';
import {
  createReducer,
  enhanceWithResetActions,
  loadingStateReducer,
  statusActionType,
} from '../../../../util/reducers';
import * as statuses from '../../../../util/statuses';
import {
  DELETE_PROFILE_ACTION_TYPE,
  LOAD_PROFILE_ACTION_TYPE,
  SUBMIT_ACTION_TYPE,
} from './actionTypes';
import confirmReducer from '../../../../form/redux/confirm/reducer';

const dependentLoadingStateReducer = (...args) =>
  enhanceWithResetActions([
    SUBMIT_ACTION_TYPE,
    LOAD_PROFILE_ACTION_TYPE,
    DELETE_PROFILE_ACTION_TYPE,
  ])(loadingStateReducer(...args));

const reducer = combineReducers({
  loadingState: dependentLoadingStateReducer(SUBMIT_ACTION_TYPE),
  preloadingState: dependentLoadingStateReducer(LOAD_PROFILE_ACTION_TYPE),
  deleteLoadingState: dependentLoadingStateReducer(DELETE_PROFILE_ACTION_TYPE),
  profile: createReducer(
    {
      [statusActionType(LOAD_PROFILE_ACTION_TYPE, statuses.SUCCESS)]: (state, { data }) => data,
      [statusActionType(SUBMIT_ACTION_TYPE, statuses.SUCCESS)]: (state, { data }) => data,
    },
    null
  ),
  confirm: confirmReducer,
});

export const initialState = reducer(undefined, {});

export default reducer;
