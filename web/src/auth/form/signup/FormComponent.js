import React, { useContext } from 'react';
import T from 'prop-types';

import { Card, Form } from 'semantic-ui-react';
import { signUp } from '../../api/authApi';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import { statusChecks } from '../../../util/statuses';
import useDataLoading from '../../../hooks/useDataLoading';

const SUBMIT_ACTION_TYPE = 'SUBMIT';

const FormComponent = ({ children }) => {
  const { handleSubmit, dispatch, state } = useContext(FormContext);

  const formInProgress = statusChecks.isInProgress(state.loadingState.status);
  const formError = statusChecks.isError(state.loadingState.status);

  const signupFn = ({ email, password, ...userData }) => signUp(email, password, userData);
  const fn = useDataLoading(SUBMIT_ACTION_TYPE, signupFn, dispatch);

  return (
    <Card fluid>
      <Card.Content>
        <Box p={3}>
          <Form onSubmit={handleSubmit(fn)} loading={formInProgress} error={formError}>
            {children}
          </Form>
        </Box>
      </Card.Content>
    </Card>
  );
};

FormComponent.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default FormComponent;
