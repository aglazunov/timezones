import React, { useReducer } from 'react';
import { useForm } from 'react-hook-form';
import T from 'prop-types';

import { Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import FirstNameField from './fields/FirstNameField';
import LastNameField from './fields/LastNameField';
import EmailField from './fields/EmailField';
import PasswordField from './fields/PasswordField';
import ConfirmPasswordField from './fields/ConfirmPasswordField';
import FormContext from '../../../form/FormContext';
import FormComponent from './FormComponent';
import SubmitButton from '../login/SubmitButton';
import { statusChecks } from '../../../util/statuses';
import Brand from '../../../components/Brand';
import Flex from '../../../components/layout/Flex';
import Box from '../../../components/layout/Box';
import reducer, { initialState } from './redux/reducer';

const defaultValues = {};

const SignupForm = ({ onSubmit }) => {
  const { register, handleSubmit, errors, watch } = useForm({
    defaultValues,
  });

  const [state, dispatch] = useReducer(reducer, initialState);
  const { loadingState } = state;

  const serverError = statusChecks.isError(loadingState.status) && loadingState.message;

  const contextValue = {
    register,
    errors,
    state,
    dispatch,
    handleSubmit,
    onSubmit,
    watch,
  };

  return (
    <FormContext.Provider value={contextValue}>
      <FormComponent>
        <Box>
          <Flex justifyContent="center" mb={4}>
            <Brand />
          </Flex>
          <Message error content={serverError} />
        </Box>

        <Box mt={4} mb={3}>
          <FirstNameField />
          <LastNameField />
          <EmailField />
          <PasswordField />
          <ConfirmPasswordField />
        </Box>

        <Box mb={3}>
          <SubmitButton data-cy="submitButton">Sign Up</SubmitButton>
        </Box>

        <Flex justifyContent="center">
          <span>Already have an account?</span>
          &nbsp;
          <Link to="/log-in" data-cy="login-link">
            Log in
          </Link>
        </Flex>
      </FormComponent>
    </FormContext.Provider>
  );
};

SignupForm.propTypes = {
  onSubmit: T.func,
};

SignupForm.defaultProps = {
  onSubmit: null,
};

export default SignupForm;
