import React, { useContext } from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';
import FormContext from '../../../../form/FormContext';

const PASSWORD_MIN_LENGTH = 1;

const fieldProps = {
  name: Fields.ConfirmPassword,
  label: 'Repeat password',
  type: 'password',
  registerProps: {
    required: 'Password confirmation is required',
    minLength: {
      value: PASSWORD_MIN_LENGTH,
      message: `Password must be at least ${PASSWORD_MIN_LENGTH}-char long`,
    },
  },
  'data-cy': 'confirmPassword',
};

const PasswordField = () => {
  const { watch } = useContext(FormContext);
  const password = watch('password');

  const props = {
    ...fieldProps,
    registerProps: {
      ...fieldProps.registerProps,
      validate: (value) => {
        if (value !== password) {
          return "Repeated password doesn't match";
        }

        return true;
      },
    },
  };

  return <FormField {...props} />;
};

export default PasswordField;
