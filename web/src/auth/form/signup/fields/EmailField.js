import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

// http://emailregex.com/
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const emailValidation = (value) => (EMAIL_REGEX.test(value) ? true : 'Not a valid email');

const fieldProps = {
  name: Fields.Email,
  label: 'Email',
  type: 'text',
  registerProps: {
    required: 'Email is required',
    validate: emailValidation,
  },
  'data-cy': 'email',
};

const EmailField = () => <FormField {...fieldProps} />;

export default EmailField;
