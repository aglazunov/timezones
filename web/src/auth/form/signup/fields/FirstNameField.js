import React from 'react';

import { Fields } from '../formConstants';
import FormField from '../../../../form/components/FormField';

const fieldProps = {
  name: Fields.FirstName,
  label: 'First Name',
  type: 'text',
  registerProps: {
    required: 'First name is required',
  },
  'data-cy': 'firstName',
};

const FirstNameField = () => <FormField {...fieldProps} />;

export default FirstNameField;
