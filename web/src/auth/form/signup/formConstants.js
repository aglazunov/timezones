export const Fields = {
  FirstName: 'firstName',
  LastName: 'lastName',
  Email: 'email',
  Password: 'password',
  ConfirmPassword: 'confirmPassword',
};
