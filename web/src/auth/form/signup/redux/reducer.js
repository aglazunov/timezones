import { combineReducers } from 'redux';
import { loadingStateReducer } from '../../../../util/reducers';
import { SUBMIT_ACTION_TYPE } from './actionTypes';

const reducer = combineReducers({
  loadingState: loadingStateReducer(SUBMIT_ACTION_TYPE),
});

export const initialState = reducer(undefined, {});
export default reducer;
