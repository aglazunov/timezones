import React from 'react';

import { ReactComponent as Logo } from './Logo.svg';
import { ReactComponent as Text } from './BrandText.svg';
import Box from './layout/Box';

const Brand = () => (
  <Box>
    <Text />
    <Logo />
  </Box>
);

export default Brand;
