import styled from 'styled-components';

const StyledButton = styled.button`
  box-shadow: 0 0;
  background: none;
  border: none;

  color: #5ea7e6;

  &:hover {
    cursor: pointer;
    color: #6891fe;
  }
`;

export default StyledButton;
