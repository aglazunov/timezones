import React, { useContext } from 'react';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import Box from './layout/Box';
import AuthContext from '../app/AuthContext';
import { isPermitted, PermissionTypes } from '../integration/permissions';

const NavFooter = () => {
  const { user } = useContext(AuthContext);

  const editProfilePermitted = isPermitted(PermissionTypes.ManageOwnUser, user.permissionLevel);
  const managePermitted = isPermitted(PermissionTypes.ManageOtherUsers, user.permissionLevel);

  if (!editProfilePermitted && !editProfilePermitted) {
    return null;
  }

  return (
    <Box flexShrink={0} bg="rgba(255, 255, 255, 0.7)">
      <Button.Group fluid basic>
        <Button icon="clock" content="Time" as={Link} to="/" data-cy="navTimeZones" />
        <Button icon="user circle" content="Profile" as={Link} to="/profile" data-cy="navProfile" />
        {managePermitted && (
          <Button icon="users" content="Users" as={Link} to="/users" data-cy="navUsers" />
        )}
      </Button.Group>
    </Box>
  );
};

export default NavFooter;
