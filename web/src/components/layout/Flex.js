import styled from 'styled-components';
import Box from './Box';

const Flex = styled(Box)`
  display: ${({ inline }) => (inline ? 'inline-flex' : 'flex')};
`;

Flex.propTypes = Box.propTypes;
Flex.defaultProps = Box.defaultProps;

export default Flex;
