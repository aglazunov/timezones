import styled from 'styled-components';
import { system } from 'styled-system';
import T from 'prop-types';
import Flex from './Flex';

const defaultScale = [1, 2, 4, 6, 8, 10, 12, 14, 16, 20, 24, 32, 48];

const px = (value) => (typeof value === 'number' ? `${value}px` : value);
const getValue = (value, scale = defaultScale) => px(scale[value] || value);

const Group = styled(Flex)`
  display: 'flex';

  > :not(:last-child) {
    ${system({
      hGap: {
        property: 'marginRight',
        scale: 'space',
        transform: getValue,
      },
      vGap: {
        property: 'marginBottom',
        scale: 'space',
        transform: getValue,
      },
    })}
  }
`;

Group.propTypes = {
  ...Flex.propTypes,
  hGap: T.oneOfType([T.string, T.number]),
  vGap: T.oneOfType([T.string, T.number]),
};
Group.defaultProps = Flex.defaultProps;

export default Group;
