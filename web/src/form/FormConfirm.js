import { Confirm } from 'semantic-ui-react';
import React, { useContext } from 'react';
import FormContext from './FormContext';

const FormConfirm = () => {
  const { state } = useContext(FormContext);
  const { confirm: confirmParams } = state;

  if (!confirmParams) {
    return null;
  }

  return <Confirm open {...confirmParams} data-cy="confirmDialog" />;
};

export default FormConfirm;
