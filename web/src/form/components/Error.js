import React from 'react';
import styled from 'styled-components';
import T from 'prop-types';

const ErrorElement = styled.div`
  color: red;
`;

const Error = ({ children }) => <ErrorElement>{children}</ErrorElement>;

Error.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default Error;
