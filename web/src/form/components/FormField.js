import React, { useContext } from 'react';
import T from 'prop-types';

import { Form, Label } from 'semantic-ui-react';
import FormContext from '../FormContext';

const FormField = ({ registerProps, name, label, type, hidden, ...props }) => {
  const { errors, register } = useContext(FormContext);
  const error = errors[name];

  return (
    <>
      <Form.Field error={Boolean(error)}>
        {label && <label htmlFor={`form-field-${name}`}>{label}</label>}
        <input
          id={`form-${name}`}
          name={name}
          type={type}
          ref={register(registerProps)}
          hidden={hidden}
          {...props}
        />
        {error && (
          <Label pointing prompt error>
            {error.message || error.type}
          </Label>
        )}
      </Form.Field>
    </>
  );
};

FormField.propTypes = {
  registerProps: T.object,
  name: T.string.isRequired,
  label: T.string.isRequired,
  type: T.string,
  hidden: T.bool,
};

FormField.defaultProps = {
  registerProps: {},
  type: 'text',
  hidden: false,
};

export default FormField;
