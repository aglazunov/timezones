import styled from 'styled-components';
import Box from '../../components/layout/Box';

const FormSizeWrapper = styled(Box)`
  padding: 1em;
  flex-grow: 1;
`;

export default FormSizeWrapper;
