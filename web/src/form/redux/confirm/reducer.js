import { createReducer } from '../../../util/reducers';
import { HIDE_MODAL, SHOW_MODAL } from './actionTypes';

const reducer = createReducer(
  {
    [SHOW_MODAL]: (state, { type, ...params }) => params,
    [HIDE_MODAL]: () => null,
  },
  null
);

export default reducer;
