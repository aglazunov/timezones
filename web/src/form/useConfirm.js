import { useContext } from 'react';
import FormContext from './FormContext';

const useConfirm = () => {
  const { dispatch } = useContext(FormContext);

  return (params) =>
    new Promise((resolve) => {
      const hideIt = () => {
        dispatch({
          type: 'HIDE_MODAL',
        });
      };

      const handleCancel = () => {
        hideIt();
        resolve(false);
      };

      const handleConfirm = () => {
        hideIt();
        resolve(true);
      };

      dispatch({
        ...params,
        onCancel: handleCancel,
        onConfirm: handleConfirm,
        type: 'SHOW_MODAL',
      });
    });
};

export default useConfirm;
