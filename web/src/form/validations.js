// http://emailregex.com/
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const email = ({ message = 'Invalid email address' } = {}) => (value = '') =>
  value && !EMAIL_REGEX.test(value) ? message : undefined;

const MIN_PASSWORD_LENGTH = 1;
const plural = MIN_PASSWORD_LENGTH !== 1;

export const password = ({
  message = `Password must have at least ${MIN_PASSWORD_LENGTH} character${plural ? 's' : ''}`,
} = {}) => (value = '') => (value.length < MIN_PASSWORD_LENGTH ? message : undefined);
