import { useRef, useEffect } from 'react';

const useAutoFocus = () => {
  const inputRef = useRef();

  // autofocus
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return inputRef;
};

export default useAutoFocus;
