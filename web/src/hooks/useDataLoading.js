import { useMemo } from 'react';
import { statusAction } from '../util/reducers';
import * as statuses from '../util/statuses';
import getApiErrorMessage from '../integration/getApiErrorMessage';

const useDataLoading = (baseActionType, asyncFn, dispatch) =>
  useMemo(
    () => async (...args) => {
      dispatch(statusAction(baseActionType, statuses.IN_PROGRESS));

      try {
        const result = await asyncFn(...args);
        dispatch(
          statusAction(baseActionType, statuses.SUCCESS, {
            data: result.data,
          })
        );
      } catch (e) {
        const errorMessage = getApiErrorMessage(e);

        dispatch(
          statusAction(baseActionType, statuses.ERROR, {
            message: errorMessage,
          })
        );
      }
    },
    [baseActionType, asyncFn, dispatch]
  );

export default useDataLoading;
