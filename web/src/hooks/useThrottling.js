import { useMemo, useRef } from 'react';
import throttle from 'lodash.throttle';

const useThrottling = (fn) => {
  const scheduleId = useRef(null);

  return useMemo(
    () =>
      throttle((...args) => {
        if (typeof scheduleId.current === 'number') {
          cancelAnimationFrame(scheduleId.current);
        }

        scheduleId.current = requestAnimationFrame(() => {
          scheduleId.current = null;
          fn(...args);
        });
      }, 100),
    [fn]
  );
};

export default useThrottling;
