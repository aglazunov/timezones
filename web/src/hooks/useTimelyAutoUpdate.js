// many components in the app will use this hook to update the time displayed.
// we optimize it in such a way that at most interval is running at a single time
import { useEffect, useState } from 'react';

let subscribers = [];

let intervalHandle;
const INTERVAL_TIME = 1000;
let lastUpdateTimeValue = '';

const updateAllSubscribers = (timeValue) => {
  subscribers.forEach((fn) => fn(timeValue));
  lastUpdateTimeValue = timeValue;
};

const checkUpdate = () => {
  const timeValue = new Date().toTimeString().substr(0, 5);

  if (lastUpdateTimeValue !== timeValue) {
    updateAllSubscribers(timeValue);
  }
};

const startInterval = () => {
  intervalHandle = setInterval(checkUpdate, INTERVAL_TIME);
};

const stopInterval = () => {
  clearInterval(intervalHandle);
};

const addSubscriber = (fn) => {
  subscribers.push(fn);

  if (subscribers.length === 1) {
    startInterval();
  }
};

const removeSubscriber = (fn) => {
  subscribers = subscribers.filter((s) => s !== fn);

  if (subscribers.length === 0) {
    stopInterval();
  }
};

const useTimelyAutoUpdate = () => {
  const [time, setTime] = useState('');

  useEffect(() => {
    addSubscriber(setTime);

    return () => {
      removeSubscriber(setTime);
    };
  }, []);

  return time;
};

export default useTimelyAutoUpdate;

export const PrecisionInterval = {
  Minutely: 'Minutely',
  Secondly: 'Secondly',
};
