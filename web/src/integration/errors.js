export const ErrorTypes = {
  NoUserId: 'NoUserId',
  PermissionDenied: 'PermissionDenied',
  ValidationError: 'ValidationError',
  GenericError: 'GenericError',
  InvalidAccessToken: 'InvalidAccessToken',
  NoSuchUser: 'NoSuchUser',
  NotFound: 'NotFound',
  DuplicateRecord: 'DuplicateRecord',
};

export const FieldValidationTypes = {
  Missing: 'Missing',
  Invalid: 'Invalid',
};
