const getApiErrorMessage = (e) => {
  let errorMessage = e.message || 'Error';

  if (e.isAxiosError) {
    const error = e.response?.data?.error || {};
    errorMessage = error.message || error.type;

    /* if (error.type === ErrorTypes.DuplicateRecord && !error.message) {
      errorMessage = 'User with this email is already registered.';
    } */
  }

  return errorMessage || 'An error has occurred.';
};

export default getApiErrorMessage;
