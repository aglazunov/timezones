// Each permission is represented by a number with a certain bit set to 1 in binary representation
export const PermissionTypes = {
  ManageOwnRecords: 1,
  ManageOwnUser: 1 << 1,
  ManageOtherRecords: 1 << 2,
  ManageOtherUsers: 1 << 3,
  ChangeOwnPermissions: 1 << 4,
  ChangeOtherPermissions: 1 << 5,
};

/*
 basic users:
  - can manage their profile
  - can't change their permissions
  - can't manage other's profile
  - can't change other's permissions
  - can't manage other's records
 */
const BasicPermissions = [PermissionTypes.ManageOwnUser, PermissionTypes.ManageOwnRecords];
const permissionReducer = (acc, value) => acc | value;

export const basicPermissions = BasicPermissions.reduce(permissionReducer, 0);
export const isPermitted = (requiredPermission, userPermissions) =>
  Boolean(requiredPermission & userPermissions);

// no user role is allowed to change their own permissions
const RolePermissions = {
  Basic: [...BasicPermissions],
  UserManager: [
    ...BasicPermissions,
    PermissionTypes.ManageOtherUsers,
    PermissionTypes.ChangeOtherPermissions,
  ],
  Admin: [
    ...BasicPermissions,
    PermissionTypes.ManageOtherUsers,
    PermissionTypes.ManageOtherRecords,
    PermissionTypes.ChangeOtherPermissions,
  ],
};

// Role permissions is a number with bits for certain permissions set to 1 in binary representation
export const Roles = Object.entries(RolePermissions).reduce((acc, [key, permissions]) => {
  acc[key] = permissions.reduce(permissionReducer, 0);

  return acc;
}, {});
