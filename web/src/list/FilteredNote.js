import T from 'prop-types';

const FilteredNote = ({ total, filtered, filterApplied, noun }) => {
  if (!filterApplied) {
    return `${total} ${noun}s.`;
  }

  const singular = filtered === 1;
  return `${filtered || 'None'} out of ${total} ${noun}s match${singular ? 'es' : ''} your search.`;
};

FilteredNote.propTypes = {
  total: T.number,
  filtered: T.number,
  filterApplied: T.bool,
  noun: T.string,
};

FilteredNote.defaultProps = {
  filterApplied: true,
  noun: 'item',
};

export default FilteredNote;
