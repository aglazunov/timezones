import { Form, Icon } from 'semantic-ui-react';
import React from 'react';
import styled from 'styled-components';
import T from 'prop-types';

const InputElement = styled.input`
  background: rgba(255, 255, 255, 0.7) !important;
`;

const ListFilter = ({ onChange }) => {
  const handleChange = (event) => onChange && onChange(event.target.value);

  return (
    <Form>
      <div className="ui icon input fluid">
        <Icon name="filter" aria-hidden="true" />
        <InputElement className="ui fluid" placeholder="Filter" onChange={handleChange} />
      </div>
    </Form>
  );
};

ListFilter.propTypes = {
  onChange: T.func,
};

ListFilter.defaultProps = {
  onChange: null,
};

export default ListFilter;
