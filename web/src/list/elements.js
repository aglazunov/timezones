import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Box from '../components/layout/Box';

const listItemStyles = `
  background: rgba(129, 135, 150, 0.4);
  padding: 8px 16px;
`;

export const ListItemBox = styled(Box)`
  ${listItemStyles}
`;

export const ListItemLink = styled(Link)`
  ${listItemStyles}

  display: block;
  color: inherit;

  &:hover {
    color: inherit;
    background: rgba(129, 135, 150, 0.6);
  }
`;
