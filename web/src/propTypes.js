import T from 'prop-types';

export const timeZonePropType = T.shape({
  name: T.string,
  alternativeName: T.string,
  group: T.arrayOf(T.string),
  continentCode: T.string,
  continentName: T.string,
  countryName: T.string,
  countryCode: T.string,
  mainCities: T.arrayOf(T.string),
  rawOffsetInMinutes: T.number,
  abbreviation: T.string,
  rawFormat: T.string,
  currentTimeOffsetInMinutes: T.number,
  currentTimeFormat: T.string,
  current: T.bool,
});
