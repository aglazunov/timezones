/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import LoginForm from '../../auth/form/login/LoginForm';
import FrameWrapper from '../../components/FrameWrapper';

export default {
  title: 'Auth/Login',
  component: LoginForm,
  argTypes: { onSubmit: { action: 'logged in' } },
  decorators: [
    (Story) => (
      <FrameWrapper>
        <Story />
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <LoginForm {...args} />;

export const Login = Template.bind({});
