/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import SignupForm from '../../auth/form/signup/SignupForm';
import FrameWrapper from '../../components/FrameWrapper';

export default {
  title: 'Auth/Signup',
  component: SignupForm,
  argTypes: { onSubmit: { action: 'signed up' } },
  decorators: [
    (Story) => (
      <FrameWrapper>
        <Story />
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <SignupForm {...args} />;

export const Signup = Template.bind({});
