/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import { getTimeZone } from '../util/storiesUtil';
import AddTimeZoneForm from '../../timezones/form/addTimeZone/AddTimeZoneForm';
import FrameWrapper from '../../components/FrameWrapper';
import AuthContext from '../../app/AuthContext';

export default {
  title: 'Time Zones/Add time zone',
  component: AddTimeZoneForm,
  argTypes: { onAdd: { action: 'added' } },
  decorators: [
    (Story) => (
      <FrameWrapper>
        <AuthContext.Provider value={{ user: {} }}>
          <Story />
        </AuthContext.Provider>
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <AddTimeZoneForm {...args} />;

export const NewYork = Template.bind({});
NewYork.args = {
  timeZone: getTimeZone('America/New_York'),
};

export const Kathmandu = Template.bind({});
Kathmandu.args = {
  timeZone: getTimeZone('Asia/Kathmandu'),
};

export const Vatican = Template.bind({});
Vatican.args = {
  timeZone: getTimeZone('Europe/Vatican'),
};
