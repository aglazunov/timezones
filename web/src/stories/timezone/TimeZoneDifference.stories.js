/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import TimeZoneDifference from '../../timezones/common/TimeZoneDifference';
import { getTimeZone } from '../util/storiesUtil';

export default {
  title: 'Time Zones/Time zone difference',
  component: TimeZoneDifference,
};

const Template = (args) => <TimeZoneDifference {...args} />;

// we're considering timezone offset constant through the app run
// though it can happen that we're looking at it when at 'spring forward' or 'fall back'
// in this case offset will change. we're not accounting for such cases
const browserOffset = new Date().getTimezoneOffset();

export const Berlin = Template.bind({});
Berlin.args = {
  timeZone: getTimeZone('Europe/Berlin'),
};

export const BerlinBrowser = Template.bind({});
BerlinBrowser.args = {
  timeZone: getTimeZone('Europe/Berlin'),
  offset: browserOffset,
};

export const Kathmandu = Template.bind({});
Kathmandu.args = {
  timeZone: getTimeZone('Asia/Kathmandu'),
};

export const KathmanduBrowser = Template.bind({});
KathmanduBrowser.args = {
  timeZone: getTimeZone('Asia/Kathmandu'),
  offset: browserOffset,
};

export const LosAngeles = Template.bind({});
LosAngeles.args = {
  timeZone: getTimeZone('America/Los_Angeles'),
};

export const LosAngelesBrowser = Template.bind({});
LosAngelesBrowser.args = {
  timeZone: getTimeZone('America/Los_Angeles'),
  offset: browserOffset,
};

export const London = Template.bind({});
London.args = {
  timeZone: getTimeZone('Europe/London'),
};

export const LondonBrowser = Template.bind({});
LondonBrowser.args = {
  timeZone: getTimeZone('Europe/London'),
  offset: browserOffset,
};

export const Minsk = Template.bind({});
Minsk.args = {
  timeZone: getTimeZone('Europe/Minsk'),
};

export const MinskBrowser = Template.bind({});
MinskBrowser.args = {
  timeZone: getTimeZone('Europe/Minsk'),
  offset: browserOffset,
};

export const Tokyo = Template.bind({});
Tokyo.args = {
  timeZone: getTimeZone('Asia/Tokyo'),
};

export const TokyoBrowser = Template.bind({});
TokyoBrowser.args = {
  timeZone: getTimeZone('Asia/Tokyo'),
  offset: browserOffset,
};
