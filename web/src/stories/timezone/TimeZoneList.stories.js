/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import TimeZoneSelector from '../../timezones/picker/TimeZoneSelector';
import TimeZoneList from '../../timezones/list/TimeZoneList';
import { getAllTimeZones, getTimeZoneByName } from '../../timezones/picker/search';
import FrameWrapper from '../../components/FrameWrapper';

export default {
  title: 'Time Zones/Time zone list',
  component: TimeZoneSelector,
  decorators: [
    (Story) => (
      <FrameWrapper>
        <Story />
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <TimeZoneList {...args} />;

// use only when len is significantly larger than nItems
// otherwise it will be very inefficient
const getNRandomIndices = (nItems, len) => {
  const result = new Set();
  let newItem;

  for (let i = 0; i < nItems; i++) {
    do {
      newItem = Math.floor(Math.random() * len);
    } while (result.has(newItem));
    result.add(newItem);
  }

  return Array.from(result);
};

const rollString = (str, len) => {
  if (str.length < len) {
    const repeats = Math.ceil(len / str.length);
    return new Array(repeats).fill(str).join('').substring(0, len);
  }

  return str;
};

const timeZoneToEntry = (timeZone) => ({
  name: timeZone.name,
  timeZone,
  city: timeZone.mainCities[0],
  id: `id-${Math.floor(Math.random() * 10000000)}`,
});

const allTimeZones = getAllTimeZones();
const randomTimeZones = (n) =>
  getNRandomIndices(n, allTimeZones.length).map((index) => timeZoneToEntry(allTimeZones[index]));

export const Random1 = Template.bind({});
Random1.args = {
  timeZones: randomTimeZones(1),
};

export const Random5 = Template.bind({});
Random5.args = {
  timeZones: randomTimeZones(5),
};

export const Random10 = Template.bind({});
Random10.args = {
  timeZones: randomTimeZones(10),
};

export const Random20 = Template.bind({});
Random20.args = {
  timeZones: randomTimeZones(20),
};

export const Full = Template.bind({});
Full.args = {
  timeZones: allTimeZones.map(timeZoneToEntry),
};

// city length can be arbitrary
export const LongCities = Template.bind({});
LongCities.args = {
  timeZones: randomTimeZones(10).map((entry, index) => ({
    ...entry,
    city:
      index > 2 && index < 8
        ? `a city which was considered so beautiful that it was named ${entry.city} by its inhabitants`
        : entry.city,
  })),
};

const LONGEST_TZ_NAME = 'America/Argentina/Buenos_Aires';
// max tz length in the database is 32. they don't have spaces
export const LongNames = Template.bind({});
LongNames.args = {
  timeZones: [timeZoneToEntry(getTimeZoneByName(LONGEST_TZ_NAME)), ...randomTimeZones(10)].map(
    (entry, index) => ({
      ...entry,
      name: index > 2 && index < 8 ? rollString(entry.name, 28 + index) : entry.name,
    })
  ),
};

export const Errorred = Template.bind({});
Errorred.args = {
  timeZones: randomTimeZones(8).map((entry, index) => ({
    ...entry,
    timeZone: index === 1 || index === 2 || index === 5 ? undefined : entry.timeZone,
  })),
};
