/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import TimeZoneLocalTime from '../../timezones/common/TimeZoneLocalTime';
import { getAllTimeZones } from '../../timezones/picker/search';

export default {
  title: 'Time Zones/Local time',
  component: TimeZoneLocalTime,
};

const Template = (args) => <TimeZoneLocalTime {...args} />;

const allTimeZones = getAllTimeZones();
const getTimeZone = (name) => allTimeZones.find((zone) => zone.name === name);

export const Berlin = Template.bind({});
Berlin.args = {
  timeZone: getTimeZone('Europe/Berlin'),
};

export const Kathmandu = Template.bind({});
Kathmandu.args = {
  timeZone: getTimeZone('Asia/Kathmandu'),
};

export const LosAngeles = Template.bind({});
LosAngeles.args = {
  timeZone: getTimeZone('America/Los_Angeles'),
};

export const London = Template.bind({});
London.args = {
  timeZone: getTimeZone('Europe/London'),
};

export const Minsk = Template.bind({});
Minsk.args = {
  timeZone: getTimeZone('Europe/Minsk'),
};

export const Tokyo = Template.bind({});
Tokyo.args = {
  timeZone: getTimeZone('Asia/Tokyo'),
};
