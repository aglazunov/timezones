/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import TimeZoneSelector from '../../timezones/picker/TimeZoneSelector';
import TimeZonesScreen from '../../timezones/list/TimeZonesScreen';
import FrameWrapper from '../../components/FrameWrapper';

export default {
  title: 'Time Zones/Time zone screen',
  component: TimeZoneSelector,
  argTypes: { onSelect: { action: 'selected' } },
  decorators: [
    (Story) => (
      <FrameWrapper>
        <Story />
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <TimeZonesScreen {...args} />;

export const Empty = Template.bind({});
Empty.args = {};
