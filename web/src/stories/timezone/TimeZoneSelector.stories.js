/* eslint-disable import/no-anonymous-default-export */

import React from 'react';
import TimeZoneSelector from '../../timezones/picker/TimeZoneSelector';
import FrameWrapper from '../../components/FrameWrapper';

export default {
  title: 'Time Zones/Time zone selector',
  component: TimeZoneSelector,
  argTypes: { onSelect: { action: 'selected' } },
  decorators: [
    (Story) => (
      <FrameWrapper>
        <Story />
      </FrameWrapper>
    ),
  ],
};

const Template = (args) => <TimeZoneSelector {...args} />;

export const Full = Template.bind({});
Full.args = {};
