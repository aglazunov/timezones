import { getAllTimeZones } from '../../timezones/picker/search';

const allTimeZones = getAllTimeZones();

export const getTimeZone = (name) => allTimeZones.find((zone) => zone.name === name);
