import React, { useState } from 'react';
import T from 'prop-types';

import CitySelector from './CitySelector';
import { timeZonePropType } from '../../propTypes';
import TimeZoneDifference from '../common/TimeZoneDifference';

const AddTimeZone = ({ timeZone, onAdd }) => {
  const [city, setCity] = useState();

  const handleAddClick = () => {
    // if no city is specified, default to most populous
    onAdd && onAdd(timeZone, city || timeZone.mainCities[0]);
  };

  const handleCityChange = (newCity) => {
    setCity(newCity);
  };

  return (
    <div>
      <div>You are adding a timezone {timeZone.name}</div>
      <div>Abbreviation: {timeZone.abbreviation}</div>
      <div>Alternative name: {timeZone.alternativeName}</div>
      <div>
        Country: {timeZone.countryName} ({timeZone.countryCode})
      </div>
      <div>
        Difference to GMT: <TimeZoneDifference timeZone={timeZone} />
      </div>
      <CitySelector timeZone={timeZone} onChange={handleCityChange} />

      <div>
        <button type="button" onClick={handleAddClick}>
          Add
        </button>
      </div>
    </div>
  );
};

AddTimeZone.propTypes = {
  timeZone: timeZonePropType.isRequired,
  onAdd: T.func,
};

AddTimeZone.defaultProps = {
  onAdd: null,
};

export default AddTimeZone;
