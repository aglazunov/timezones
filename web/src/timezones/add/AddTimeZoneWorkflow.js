import React, { useState } from 'react';

import TimeZoneSelector from '../picker/TimeZoneSelector';
import AddTimeZoneForm from '../form/addTimeZone/AddTimeZoneForm';

const AddTimeZoneWorkflow = () => {
  const [timeZone, setTimeZone] = useState(null);

  if (!timeZone) {
    return <TimeZoneSelector onSelect={setTimeZone} />;
  }

  return <AddTimeZoneForm timeZone={timeZone} />;
};

export default AddTimeZoneWorkflow;
