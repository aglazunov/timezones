import React, { useState } from 'react';
import T from 'prop-types';
import { Input } from 'semantic-ui-react';

import { timeZonePropType } from '../../propTypes';
import Box from '../../components/layout/Box';
import MainCities from './MainCities';

const CitySelector = ({ timeZone, onChange }) => {
  const [city, setCity] = useState(timeZone ? timeZone.mainCities[0] : null);

  const handleChange = (event) => {
    const newCity = event.target.value;

    setCity(newCity);
    onChange && onChange(newCity);
  };

  const handleMainCityChange = (newCity) => {
    setCity(newCity);
    onChange && onChange(newCity);
  };

  return (
    <div>
      <Box fontSize={3} mb={2}>
        <Input fluid value={city} onChange={handleChange} data-cy="city" />
      </Box>
      <div>
        <MainCities onChange={handleMainCityChange} />
      </div>
    </div>
  );
};

CitySelector.propTypes = {
  timeZone: timeZonePropType,
  onChange: T.func,
};

CitySelector.defaultProps = {
  onChange: null,
  timeZone: null,
};

export default CitySelector;
