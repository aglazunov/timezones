import React from 'react';
import T from 'prop-types';

import { timeZonePropType } from '../../propTypes';
import Group from '../../components/layout/Group';
import LinkButton from '../../components/LinkButton';

const MainCities = ({ timeZone, onChange }) => {
  const handleCityClick = (event, newCity) => {
    event.preventDefault();
    onChange && onChange(newCity);
  };

  if (!timeZone) {
    return null;
  }

  return (
    <Group hGap={4} flexWrap="wrap">
      <span>Main cities:</span>
      {timeZone.mainCities.map((mainCity) => (
        <LinkButton key={mainCity} onClick={(event) => handleCityClick(event, mainCity)}>
          {mainCity}
        </LinkButton>
      ))}
    </Group>
  );
};

MainCities.propTypes = {
  timeZone: timeZonePropType,
  onChange: T.func,
};

MainCities.defaultProps = {
  onChange: null,
  timeZone: null,
};

export default MainCities;
