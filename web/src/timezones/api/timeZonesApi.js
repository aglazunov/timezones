import axios from 'axios';
import { API_ENDPOINT } from '../../config';

export const loadTimeZones = (userId) => {
  if (!userId) {
    return axios.get(`${API_ENDPOINT}/timeZones`);
  }

  return axios.get(`${API_ENDPOINT}/users/${userId}/timeZones`);
};

export const loadTimeZone = (id) => axios.get(`${API_ENDPOINT}/timeZones/${id}`);

export const updateTimeZone = (id, values) =>
  axios.patch(`${API_ENDPOINT}/timeZones/${id}`, values);

export const createTimeZone = (userId, values) =>
  axios.post(`${API_ENDPOINT}/users/${userId}/timeZones`, values);

export const deleteTimeZone = (id) => axios.delete(`${API_ENDPOINT}/timeZones/${id}`);
