import T from 'prop-types';

import { timeZonePropType } from '../../propTypes';

const padStr = (str, len, char = '0') => {
  if (str.length < len) {
    return new Array(len - str.length).fill(char).join('') + str;
  }

  return str;
};

export const minutesRepresentation = (minutes) => {
  let sign = '';
  if (minutes >= 0) {
    sign = '+';
  } else if (minutes < 0) {
    sign = '-';
  }

  const absMinutes = Math.abs(minutes);
  const hours = Math.floor(absMinutes / 60);
  const remainder = absMinutes % 60;

  return `${sign}${hours}:${padStr(String(remainder), 2)}`;
};

const TimeZoneDifference = ({ timeZone, offset = 0 }) =>
  minutesRepresentation(timeZone.currentTimeOffsetInMinutes + offset);

TimeZoneDifference.propTypes = {
  timeZone: timeZonePropType.isRequired,
  offset: T.number,
};

export default TimeZoneDifference;
