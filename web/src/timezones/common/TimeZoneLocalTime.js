import T from 'prop-types';
import { useMemo } from 'react';

import { timeZonePropType } from '../../propTypes';
import useTimelyAutoUpdate, { PrecisionInterval } from '../../hooks/useTimelyAutoUpdate';

const TimeZoneLocalTime = ({ timeZone, precisionInterval = PrecisionInterval.Minutely }) => {
  // we'll be updating each time the minute change
  useTimelyAutoUpdate(PrecisionInterval.Minutely);

  const dateTimeFormat = useMemo(
    () =>
      new Intl.DateTimeFormat(undefined, {
        hour: 'numeric',
        minute: 'numeric',
        second: precisionInterval === PrecisionInterval.Secondly ? 'numeric' : undefined,
        timeZone: timeZone.name,
        hour12: false,
      }),
    [timeZone, precisionInterval]
  );

  return dateTimeFormat.format(new Date());
};

TimeZoneLocalTime.propTypes = {
  timeZone: timeZonePropType.isRequired,
  precisionInterval: T.oneOf([PrecisionInterval.Minutely, PrecisionInterval.Secondly]),
};

export default TimeZoneLocalTime;
