import styled, { css } from 'styled-components';
import { Label } from 'semantic-ui-react';

export const ErrorLabel = styled(Label)`
  ${(props) =>
    props.fluid &&
    css`
      width: 100%;
    `}
  background: rgba(201, 97, 73, 0.8) !important;
  font-weight: normal !important;
  color: #ffffff !important;
`;
