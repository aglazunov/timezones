import { minutesRepresentation } from './TimeZoneDifference';

test('minutesRepresentation', () => {
  expect(minutesRepresentation(60)).toEqual('+1:00');
  expect(minutesRepresentation(0)).toEqual('+0:00');
  expect(minutesRepresentation(-120)).toEqual('-2:00');
  expect(minutesRepresentation(-45)).toEqual('-0:45');
  expect(minutesRepresentation(-105)).toEqual('-1:45');
  expect(minutesRepresentation(390)).toEqual('+6:30');
});
