import React from 'react';
import { Button } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import Box from '../../../components/layout/Box';
import Flex from '../../../components/layout/Flex';
import SubmitButton from '../../../auth/form/login/SubmitButton';

const ActionButtons = () => {
  const history = useHistory();

  const handleCloseClick = (event) => {
    event.preventDefault();

    history.goBack();
  };

  return (
    <Flex flexDirection="column">
      <Box mb={1}>
        <SubmitButton data-cy="submitButton">Add</SubmitButton>
      </Box>
      <Box mb={1}>
        <Button data-cy="cancelButton" fluid basic secondary onClick={handleCloseClick}>
          Cancel
        </Button>
      </Box>
    </Flex>
  );
};

export default ActionButtons;
