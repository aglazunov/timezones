import React, { useEffect, useReducer } from 'react';

import { useForm } from 'react-hook-form';
import { Message } from 'semantic-ui-react';
import Flex from '../../../components/layout/Flex';
import { ReactComponent as Logo } from '../../../components/Logo.svg';
import reducer, { initialState } from './redux/reducer';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import ActionButtons from './ActionButtons';
import FormConfirm from '../../../form/FormConfirm';
import FormComponent from './FormComponent';
import { statusChecks } from '../../../util/statuses';
import TimeZoneFields from '../common/TimeZoneFields';
import { SET_DATA } from './redux/actionTypes';
import { timeZonePropType } from '../../../propTypes';

const AddTimeZoneForm = ({ timeZone }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { loadingState } = state;

  useEffect(() => {
    dispatch({
      type: SET_DATA,
      data: {
        name: timeZone.name,
        timeZone,
      },
    });
  }, [timeZone]);

  const { register, handleSubmit, errors, setValue } = useForm({
    defaultValues: {
      city: timeZone.mainCities[0],
    },
  });

  const serverError = statusChecks.isError(loadingState.status) && loadingState.message;
  const contextValue = {
    register,
    errors,
    state,
    dispatch,
    setValue,
    handleSubmit,
  };

  return (
    <FormContext.Provider value={contextValue}>
      <FormComponent>
        <Flex fontSize={4} alignItems="center" justifyContent="space-between">
          <span>Add time zone</span>
          <Logo />
        </Flex>

        <Message error content={serverError} />

        <TimeZoneFields />

        <Box mt={2}>
          <ActionButtons />
        </Box>

        <FormConfirm />
      </FormComponent>
    </FormContext.Provider>
  );
};

AddTimeZoneForm.propTypes = {
  timeZone: timeZonePropType.isRequired,
};

export default AddTimeZoneForm;
