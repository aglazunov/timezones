import React, { useContext, useMemo } from 'react';
import T from 'prop-types';
import { useParams, useHistory } from 'react-router-dom';

import { Card, Form } from 'semantic-ui-react';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import useDataLoading from '../../../hooks/useDataLoading';
import { SUBMIT_ACTION_TYPE } from './redux/actionTypes';
import { createTimeZone } from '../../api/timeZonesApi';
import { statusChecks } from '../../../util/statuses';
import AuthContext from '../../../app/AuthContext';

const FormComponent = ({ children }) => {
  const { handleSubmit, state, dispatch } = useContext(FormContext);
  const { loadingState } = state;
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const userId = useParams().userId || user.id;

  const createFn = useMemo(
    () => async (values) => {
      const { data } = state;

      const result = await createTimeZone(userId, {
        ...data,
        ...values,
      });

      history.goBack();

      return result;
    },
    [history, state, userId]
  );
  const onSubmit = useDataLoading(SUBMIT_ACTION_TYPE, createFn, dispatch);

  return (
    <Card fluid>
      <Card.Content>
        <Box p={3}>
          <Form
            onSubmit={handleSubmit(onSubmit)}
            loading={statusChecks.isInProgress(loadingState.status)}
            error={statusChecks.isError(loadingState.status)}
          >
            {children}
          </Form>
        </Box>
      </Card.Content>
    </Card>
  );
};

FormComponent.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default FormComponent;
