import { combineReducers } from 'redux';
import { createReducer, loadingStateReducer } from '../../../../util/reducers';
import { SET_DATA, SUBMIT_ACTION_TYPE } from './actionTypes';
import confirmReducer from '../../../../form/redux/confirm/reducer';

const reducer = combineReducers({
  loadingState: loadingStateReducer(SUBMIT_ACTION_TYPE),
  data: createReducer(
    {
      [SET_DATA]: (state, { data }) => data,
    },
    null
  ),
  confirm: confirmReducer,
});

export const initialState = reducer(undefined, {});

export default reducer;
