import React, { useContext } from 'react';
import Box from '../../../components/layout/Box';
import Flex from '../../../components/layout/Flex';
import TimeZoneTimeIndicator from '../editTimeZone/TimeZoneTimeIndicator';
import InvalidTimeZoneBanner from '../editTimeZone/InvalidTimeZoneBanner';
import NameField from './fields/NameField';
import AlternativeNameField from './fields/AlternativeNameField';
import AbbreviationField from './fields/AbbreviationField';
import CountryField from './fields/CountryField';
import CityField from './fields/CityField';
import FormContext from '../../../form/FormContext';

const TimeZoneFields = () => {
  const { state } = useContext(FormContext);
  const { data } = state;

  if (!data) {
    return null;
  }

  return (
    <Box>
      <Flex justifyContent="space-between">
        <NameField />
        <Box flexShrink={0}>
          <TimeZoneTimeIndicator timeZone={data.timeZone} />
        </Box>
      </Flex>

      {!data.timeZone && (
        <Box mb={2}>
          <InvalidTimeZoneBanner />
        </Box>
      )}

      {data.timeZone && (
        <>
          <AlternativeNameField />
          <AbbreviationField />
          <CountryField />
        </>
      )}

      <CityField />
    </Box>
  );
};

export default TimeZoneFields;
