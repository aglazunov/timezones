import { Form, Label } from 'semantic-ui-react';
import React, { useContext } from 'react';
import ReadOnlyField from './ReadOnlyField';
import FormContext from '../../../../form/FormContext';

const AbbreviationField = () => {
  const { state } = useContext(FormContext);
  const { timeZone } = state.data;

  return (
    <Form.Field>
      <label htmlFor="form-field-name">Abbreviation</label>
      <Label color="grey">
        <ReadOnlyField>{timeZone.abbreviation}</ReadOnlyField>
      </Label>
    </Form.Field>
  );
};

export default AbbreviationField;
