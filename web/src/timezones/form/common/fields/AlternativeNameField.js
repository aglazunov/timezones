import { Form } from 'semantic-ui-react';
import React, { useContext } from 'react';
import ReadOnlyField from './ReadOnlyField';
import FormContext from '../../../../form/FormContext';

const AlternativeNameField = () => {
  const { state } = useContext(FormContext);
  const { timeZone } = state.data;

  return (
    <Form.Field>
      <label htmlFor="form-field-name">Alternative Name</label>
      <ReadOnlyField>{timeZone.alternativeName}</ReadOnlyField>
    </Form.Field>
  );
};

export default AlternativeNameField;
