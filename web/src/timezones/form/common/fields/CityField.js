import React, { useContext } from 'react';
import styled from 'styled-components';
import FormField from '../../../../form/components/FormField';
import MainCities from '../../../add/MainCities';
import FormContext from '../../../../form/FormContext';

const fieldProps = {
  name: 'city',
  label: 'City',
  type: 'text',
  registerProps: {
    required: 'City is required',
  },
  'data-cy': 'city',
};

const Wrapper = styled.div`
  input {
    font-size: 20px !important;
  }
`;

const CityField = () => {
  const { state, setValue } = useContext(FormContext);
  const { timeZone } = state.data;

  const handleMainCityChange = (city) => {
    setValue('city', city, {
      shouldDirty: true,
    });
  };

  return (
    <Wrapper>
      <FormField {...fieldProps} />
      <MainCities timeZone={timeZone} onChange={handleMainCityChange} />
    </Wrapper>
  );
};

export default CityField;
