import { Form } from 'semantic-ui-react';
import React, { useContext } from 'react';
import ReadOnlyField from './ReadOnlyField';
import FormContext from '../../../../form/FormContext';
import CountryFlag from '../../../common/CountryFlag';

const CountryField = () => {
  const { state } = useContext(FormContext);
  const { timeZone } = state.data;

  return (
    <Form.Field>
      <label htmlFor="form-field-name">Country</label>
      <ReadOnlyField>
        <CountryFlag timeZone={timeZone} />
        {timeZone.countryName} ({timeZone.countryCode})
      </ReadOnlyField>
    </Form.Field>
  );
};

export default CountryField;
