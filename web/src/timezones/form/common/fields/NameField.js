import { Form } from 'semantic-ui-react';
import React, { useContext } from 'react';
import ReadOnlyField from './ReadOnlyField';
import FormContext from '../../../../form/FormContext';

const NameField = () => {
  const { state } = useContext(FormContext);
  const { name } = state.data;

  return (
    <Form.Field>
      <label htmlFor="form-field-name">Time Zone Name</label>
      <ReadOnlyField>{name}</ReadOnlyField>
    </Form.Field>
  );
};

export default NameField;
