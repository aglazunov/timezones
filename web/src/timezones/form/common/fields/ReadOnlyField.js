import React from 'react';
import T from 'prop-types';
import styled from 'styled-components';

import Box from '../../../../components/layout/Box';

const WordBreak = styled(Box)`
  word-break: break-all;
`;

const ReadOnlyField = ({ children }) => <WordBreak fontSize={3}>{children}</WordBreak>;

ReadOnlyField.propTypes = {
  children: T.oneOfType([T.arrayOf(T.node), T.node]).isRequired,
};

export default ReadOnlyField;
