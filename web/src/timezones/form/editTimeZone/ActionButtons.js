import React, { useContext, useMemo } from 'react';
import { Button } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import Box from '../../../components/layout/Box';
import Flex from '../../../components/layout/Flex';
import FormContext from '../../../form/FormContext';
import useDataLoading from '../../../hooks/useDataLoading';
import { DELETE_ACTION_TYPE } from './redux/actionTypes';
import * as api from '../../api/timeZonesApi';
import useConfirm from '../../../form/useConfirm';
import { statusChecks } from '../../../util/statuses';
import SubmitButton from '../../../auth/form/login/SubmitButton';

const ActionButtons = () => {
  const { formState, dispatch, state, timeZoneId } = useContext(FormContext);
  const name = state?.data?.name;
  const { preloadingState } = state;
  const confirmDialog = useConfirm();
  const history = useHistory();

  const deleteTimeZone = useMemo(() => () => api.deleteTimeZone(timeZoneId), [timeZoneId]);
  const deleteFn = useDataLoading(DELETE_ACTION_TYPE, deleteTimeZone, dispatch);

  const loadedOk = statusChecks.isSuccess(preloadingState.status);

  const handleRemoveClick = async () => {
    const confirmed = await confirmDialog({
      content: `Please confirm deleting time zone ${name}`,
      cancelButton: 'Cancel',
      confirmButton: 'Delete',
    });

    if (confirmed) {
      await deleteFn();
      history.goBack();
    }
  };

  const handleCloseClick = async () => {
    let confirmed = true;

    if (formState.isDirty) {
      confirmed = await confirmDialog({
        content: 'You have unsaved settings. Sure to discard them?',
        cancelButton: 'No',
        confirmButton: 'Yes',
      });
    }

    if (confirmed) {
      history.goBack();
    }
  };

  return (
    <Flex flexDirection="column">
      {loadedOk && (
        <Box mb={4}>
          <Button fluid color="red" onClick={handleRemoveClick} data-cy="deleteButton">
            Delete Time Zone
          </Button>
        </Box>
      )}
      {loadedOk && formState.isDirty && (
        <Box mb={1}>
          <SubmitButton>Save</SubmitButton>
        </Box>
      )}
      <Box mb={1}>
        <Button fluid basic secondary onClick={handleCloseClick}>
          Close
        </Button>
      </Box>
    </Flex>
  );
};

export default ActionButtons;
