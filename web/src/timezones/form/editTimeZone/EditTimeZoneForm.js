import React, { useEffect, useMemo, useReducer } from 'react';

import { useForm } from 'react-hook-form';
import { Message } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import Flex from '../../../components/layout/Flex';
import { ReactComponent as Logo } from '../../../components/Logo.svg';
import reducer, { initialState } from './redux/reducer';
import FormContext from '../../../form/FormContext';
import Box from '../../../components/layout/Box';
import useDataLoading from '../../../hooks/useDataLoading';
import { LOAD_ACTION_TYPE } from './redux/actionTypes';
import ActionButtons from './ActionButtons';
import FormConfirm from '../../../form/FormConfirm';
import { getErrorMessage } from './redux/selectors';
import FormComponent from './FormComponent';
import { loadTimeZone } from '../../api/timeZonesApi';
import TimeZoneFields from '../common/TimeZoneFields';

const EditTimeZoneForm = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { data } = state;
  const { register, handleSubmit, errors, reset, setValue, formState } = useForm({
    defaultValues: {
      city: data?.city || '',
    },
  });
  const { timeZoneId } = useParams();
  const getTimeZoneData = useMemo(() => () => loadTimeZone(timeZoneId), [timeZoneId]);
  const fetchData = useDataLoading(LOAD_ACTION_TYPE, getTimeZoneData, dispatch);
  useEffect(() => fetchData(), [fetchData]);
  const serverError = getErrorMessage(state);
  const contextValue = {
    register,
    errors,
    state,
    dispatch,
    setValue,
    handleSubmit,
    formState,
    timeZoneId,
  };

  useEffect(() => {
    reset({
      city: data?.city || '',
    });
  }, [data, reset]);

  return (
    <FormContext.Provider value={contextValue}>
      <FormComponent>
        <Flex fontSize={4} alignItems="center" justifyContent="space-between">
          <span>Edit time zone</span>
          <Logo />
        </Flex>

        <Message error content={serverError} />

        <TimeZoneFields />

        <Box mt={2}>
          <ActionButtons />
        </Box>

        <FormConfirm />
      </FormComponent>
    </FormContext.Provider>
  );
};

export default EditTimeZoneForm;
