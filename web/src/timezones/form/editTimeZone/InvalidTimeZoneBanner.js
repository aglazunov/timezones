import { Icon } from 'semantic-ui-react';
import React from 'react';
import Flex from '../../../components/layout/Flex';
import { ErrorLabel } from '../../common/elements';

const InvalidTimeZoneBanner = () => (
  <ErrorLabel fluid size="large">
    <Flex alignItems="center">
      <Icon size="large" name="exclamation circle" />
      <span>Invalid time zone. Removal recommended.</span>
    </Flex>
  </ErrorLabel>
);

export default InvalidTimeZoneBanner;
