import { Label } from 'semantic-ui-react';
import React from 'react';
import Flex from '../../../components/layout/Flex';
import Box from '../../../components/layout/Box';
import TimeZoneLocalTime from '../../common/TimeZoneLocalTime';
import TimeZoneDifference from '../../common/TimeZoneDifference';
import { timeZonePropType } from '../../../propTypes';

const TimeZoneTimeIndicator = ({ timeZone }) => (
  <Box>
    {timeZone && (
      <Flex inline flexDirection="column">
        <Box fontSize={5} mb={2} mt={2}>
          <TimeZoneLocalTime timeZone={timeZone} />
        </Box>
        <Label color="teal" size="large">
          GMT
          <TimeZoneDifference timeZone={timeZone} />
        </Label>
      </Flex>
    )}
  </Box>
);

TimeZoneTimeIndicator.propTypes = {
  timeZone: timeZonePropType.isRequired,
};

export default TimeZoneTimeIndicator;
