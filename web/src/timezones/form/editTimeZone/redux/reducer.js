import { combineReducers } from 'redux';
import {
  createReducer,
  enhanceWithResetActions,
  loadingStateReducer,
  statusActionType,
} from '../../../../util/reducers';
import * as statuses from '../../../../util/statuses';
import { SUBMIT_ACTION_TYPE, LOAD_ACTION_TYPE, DELETE_ACTION_TYPE } from './actionTypes';
import confirmReducer from '../../../../form/redux/confirm/reducer';
import { getTimeZoneByName } from '../../../picker/search';

const dependentLoadingStateReducer = (...args) =>
  enhanceWithResetActions([SUBMIT_ACTION_TYPE, LOAD_ACTION_TYPE, DELETE_ACTION_TYPE])(
    loadingStateReducer(...args)
  );

const updateData = (state, { data }) => ({
  ...data,
  timeZone: getTimeZoneByName(data.name),
});

const reducer = combineReducers({
  loadingState: dependentLoadingStateReducer(SUBMIT_ACTION_TYPE),
  preloadingState: dependentLoadingStateReducer(LOAD_ACTION_TYPE),
  deleteLoadingState: dependentLoadingStateReducer(DELETE_ACTION_TYPE),
  data: createReducer(
    {
      [statusActionType(LOAD_ACTION_TYPE, statuses.SUCCESS)]: updateData,
      [statusActionType(SUBMIT_ACTION_TYPE, statuses.SUCCESS)]: updateData,
    },
    null
  ),
  confirm: confirmReducer,
});

export const initialState = reducer(undefined, {});

export default reducer;
