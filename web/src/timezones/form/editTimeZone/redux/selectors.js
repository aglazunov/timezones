import { statusChecks } from '../../../../util/statuses';

export const isInProgress = (state) =>
  statusChecks.isInProgress(state.loadingState.status) ||
  statusChecks.isInProgress(state.preloadingState.status) ||
  statusChecks.isInProgress(state.deleteLoadingState.status);

export const isError = (state) =>
  statusChecks.isError(state.loadingState.status) ||
  statusChecks.isError(state.preloadingState.status) ||
  statusChecks.isError(state.deleteLoadingState.status);

export const getErrorMessage = (state) => {
  if (isError(state)) {
    return (
      state.loadingState.message ||
      state.preloadingState.message ||
      state.deleteLoadingState.message
    );
  }

  return null;
};
