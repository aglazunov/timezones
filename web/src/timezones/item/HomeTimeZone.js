import styled from 'styled-components';
import React, { useMemo } from 'react';
import { Label } from 'semantic-ui-react';
import { getAllTimeZones } from '../picker/search';
import TimeZoneLocalTime from '../common/TimeZoneLocalTime';
import Flex from '../../components/layout/Flex';
import Box from '../../components/layout/Box';
import { ListItemBox } from '../../list/elements';

const UpperText = styled.span`
  text-transform: uppercase;
`;

const NameElement = styled(Box)`
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const HomeTimeZone = () => {
  const timeZone = useMemo(() => getAllTimeZones().find(({ current }) => current), []);

  if (!timeZone) {
    return null;
  }

  return (
    <ListItemBox>
      <Flex alignItems="center">
        <Flex flexDirection="column" alignItems="left" flexGrow={1}>
          <NameElement fontSize={3}>
            <span title={timeZone.name}>{timeZone.name}</span>
          </NameElement>
          <Box>
            {timeZone.abbreviation} - {timeZone.alternativeName}
          </Box>
        </Flex>
        <Box flexShrink={0}>
          <Label color="yellow">
            <Flex flexDirection="column" alignItems="center">
              <UpperText>Local</UpperText>
              <UpperText>Time</UpperText>
            </Flex>
          </Label>
        </Box>
        <Box ml={1} fontSize={5} flexShrink={0}>
          <TimeZoneLocalTime timeZone={timeZone} />
        </Box>
      </Flex>
    </ListItemBox>
  );
};

export default HomeTimeZone;
