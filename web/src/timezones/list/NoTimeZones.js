import { Button, Card } from 'semantic-ui-react';
import React from 'react';
import T from 'prop-types';
import { Link } from 'react-router-dom';

import Box from '../../components/layout/Box';

const NoTimeZones = ({ userId }) => (
  <Card fluid>
    <Card.Content>
      <Box fontSize="1.2rem" textAlign="center" my={3}>
        {!userId && (
          <>
            <Box mb={1}>Your added time zones</Box>
            <Box>will appear here</Box>
          </>
        )}
        {userId && (
          <>
            <Box mb={1}>This user hasn&apos;t added</Box>
            <Box>any time zones</Box>
          </>
        )}
      </Box>
      <Box mb={2}>
        <Button
          as={Link}
          primary
          size="large"
          fluid
          to={userId ? `/users/${userId}/time-zones/add` : '/add'}
          data-cy="addTimeZone"
        >
          Add Time Zone
        </Button>
      </Box>
    </Card.Content>
  </Card>
);

NoTimeZones.propTypes = {
  userId: T.string,
};

NoTimeZones.defaultProps = {
  userId: null,
};

export default NoTimeZones;
