import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import T from 'prop-types';

import { ListItemBox } from '../../list/elements';
import { loadUserProfile } from '../../users/api/userApi';
import Box from '../../components/layout/Box';

const WordBreak = styled(Box)`
  word-break: break-all;
`;

const OtherUserInfo = ({ userId }) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const profile = await loadUserProfile(userId);
      setUser(profile.data);
    };

    if (userId) {
      fetchData();
    }
  }, [userId]);

  return (
    <ListItemBox>
      <WordBreak>
        You are viewing time zones for{' '}
        {user ? (
          <span>
            {user.firstName} {user.lastName} ({user.email})
          </span>
        ) : (
          'other user'
        )}
      </WordBreak>
    </ListItemBox>
  );
};

OtherUserInfo.propTypes = {
  userId: T.string.isRequired,
};

export default OtherUserInfo;
