import React from 'react';
import T from 'prop-types';

import TimeZoneListItem from './TimeZoneListItem';
import Group from '../../components/layout/Group';
import { timeZonePropType } from '../../propTypes';

const TimeZoneList = ({ timeZones }) => (
  <Group vGap="1px" flexDirection="column" data-cy="timeZoneList">
    {timeZones.map(({ timeZone, name, city, id }) => (
      <TimeZoneListItem key={id} timeZone={timeZone} name={name} city={city} timeZoneId={id} />
    ))}
  </Group>
);

TimeZoneList.propTypes = {
  timeZones: T.arrayOf(timeZonePropType).isRequired,
};

export default TimeZoneList;
