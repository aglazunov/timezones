import React from 'react';
import { Label, Icon } from 'semantic-ui-react';
import T from 'prop-types';
import styled from 'styled-components';

import { ErrorLabel } from '../common/elements';
import Flex from '../../components/layout/Flex';
import Box from '../../components/layout/Box';
import TimeZoneLocalTime from '../common/TimeZoneLocalTime';
import TimeZoneDifference from '../common/TimeZoneDifference';
import { timeZonePropType } from '../../propTypes';
import { ListItemLink } from '../../list/elements';

const browserOffset = new Date().getTimezoneOffset();

const NameElement = styled(Box)`
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const TimeZoneListItem = ({ timeZoneId, timeZone, city, name }) => {
  const available = timeZone instanceof Object;

  return (
    <ListItemLink to={`/time-zones/${timeZoneId}`}>
      <Flex alignItems="center">
        <Flex flexDirection="column" alignItems="left" flexGrow={1}>
          <Flex pr={1}>
            <NameElement mr={1}>
              <span title={name}>{name}</span>
            </NameElement>
            {available && (
              <Label size="mini" color="grey">
                {timeZone.abbreviation}
              </Label>
            )}
          </Flex>
          <Box fontSize={3}>{city}</Box>
        </Flex>
        {available && (
          <>
            <Box flexShrink={0}>
              <Label color="teal" size="large">
                <TimeZoneDifference timeZone={timeZone} offset={browserOffset} />
              </Label>
            </Box>
            <Box ml={1} fontSize={5} flexShrink={0}>
              <TimeZoneLocalTime timeZone={timeZone} />
            </Box>
          </>
        )}
        {!available && (
          <Box flexShrink={0}>
            <ErrorLabel size="large">
              <Flex alignItems="center">
                <Icon size="large" name="exclamation circle" />
                <span>Invalid time zone</span>
              </Flex>
            </ErrorLabel>
          </Box>
        )}
      </Flex>
    </ListItemLink>
  );
};

TimeZoneListItem.propTypes = {
  timeZoneId: T.string.isRequired,
  timeZone: timeZonePropType,
  city: T.string.isRequired,
  name: T.string.isRequired,
};

TimeZoneListItem.defaultProps = {
  timeZone: null,
};

export default TimeZoneListItem;
