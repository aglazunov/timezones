import React, { useContext, useEffect, useReducer } from 'react';
import { Loader, Button, Message } from 'semantic-ui-react';
import { Link, useParams } from 'react-router-dom';

import Box from '../../components/layout/Box';
import HomeTimeZone from '../item/HomeTimeZone';
import NoTimeZones from './NoTimeZones';
import * as api from '../api/timeZonesApi';
import useDataLoading from '../../hooks/useDataLoading';
import reducer from './redux/reducer';
import { getFilter, getFilteredTimezones, getTimeZones } from './redux/selectors';
import { LOAD_DATA_ACTION_TYPE, SET_FILTER_ACTION_TYPE } from './redux/actionTypes';
import TimeZoneList from './TimeZoneList';
import { statusChecks } from '../../util/statuses';
import Flex from '../../components/layout/Flex';
import ListFilter from '../../list/ListFilter';
import FilteredNote from '../../list/FilteredNote';
import OtherUserInfo from './OtherUserInfo';
import AuthContext from '../../app/AuthContext';

const initialState = reducer(undefined, {});

const TimeZonesScreen = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const timeZones = getTimeZones(state);
  const filter = getFilter(state);
  const { loadingState } = state;
  let { userId } = useParams();
  const userContextValue = useContext(AuthContext);

  // if we're the same user, set userId to null
  if (userContextValue?.user?.id === userId) {
    userId = null;
  }

  const timeZonesAvailable = timeZones instanceof Array && timeZones.length > 0;
  const filteredTimeZones = getFilteredTimezones(state);

  const fetchData = useDataLoading(LOAD_DATA_ACTION_TYPE, api.loadTimeZones, dispatch);
  useEffect(() => {
    fetchData(userId);
  }, [fetchData, userId]);

  const handleFilterChange = (value) => {
    dispatch({
      type: SET_FILTER_ACTION_TYPE,
      filter: value,
    });
  };

  return (
    <Box flex="1 0 auto">
      <Box mb={3}>
        {userId && <OtherUserInfo userId={userId} />}
        {!userId && <HomeTimeZone />}
      </Box>
      {statusChecks.isError(loadingState.status) && (
        <Box px={3}>
          <Message error content={loadingState.message} />
        </Box>
      )}
      {statusChecks.isInProgress(loadingState.status) && (
        <Flex justifyContent="center" mb={3}>
          <Loader active inline />
        </Flex>
      )}
      {statusChecks.isSuccess(loadingState.status) && timeZonesAvailable && (
        <>
          <Box px={3} mb={3}>
            <Button
              as={Link}
              primary
              size="large"
              fluid
              to={userId ? `/users/${userId}/time-zones/add` : '/add'}
              data-cy="addTimeZone"
            >
              Add Time Zone
            </Button>
          </Box>

          <ListFilter onChange={handleFilterChange} />
          <TimeZoneList timeZones={filteredTimeZones} />

          {filter && (
            <Box p={3}>
              <Message>
                <FilteredNote
                  total={timeZones.length}
                  filtered={filteredTimeZones.length}
                  filterApplied={Boolean(filter)}
                  noun="time zone"
                />
              </Message>
            </Box>
          )}
        </>
      )}
      {statusChecks.isSuccess(loadingState.status) && !timeZonesAvailable && (
        <Box px={3} py={3}>
          <NoTimeZones userId={userId} />
        </Box>
      )}
    </Box>
  );
};

export default TimeZonesScreen;
