import { combineReducers } from 'redux';
import sortOn from 'sort-on';

import { createReducer, loadingStateReducer, statusActionType } from '../../../util/reducers';
import * as statuses from '../../../util/statuses';
import { getTimeZoneByName } from '../../picker/search';
import { LOAD_DATA_ACTION_TYPE, SET_FILTER_ACTION_TYPE } from './actionTypes';

const reducer = combineReducers({
  loadingState: loadingStateReducer(LOAD_DATA_ACTION_TYPE),
  timeZones: createReducer(
    {
      [statusActionType(LOAD_DATA_ACTION_TYPE, statuses.SUCCESS)]: (state, { data }) =>
        sortOn(
          data.map((timeZone) => ({
            id: timeZone._id,
            city: timeZone.city,
            name: timeZone.name,
            timeZone: getTimeZoneByName(timeZone.name),
            key: timeZone.compoundKey,
          })),
          ['timeZone.currentTimeOffsetInMinutes', 'name', 'city']
        ),
    },
    null
  ),
  filter: createReducer(
    {
      [SET_FILTER_ACTION_TYPE]: (state, { filter }) => filter,
    },
    null
  ),
});

export default reducer;
