import { createSelector } from 'reselect';

export const getTimeZones = (state) => state.timeZones;
export const getFilter = (state) => state.filter;

const properties = ['name', 'abbreviation'];

const filterMatcher = (filter) => ({ name = '', city = '', timeZone }) => {
  const lowerCaseFilter = filter.toLowerCase();

  if (city.toLowerCase().indexOf(lowerCaseFilter) >= 0) {
    return true;
  }

  // sometimes time zones can be invalid – don't have timeZone entry. filter them by name only
  if (!timeZone) {
    return name.toLowerCase().indexOf(lowerCaseFilter) >= 0;
  }

  return properties.some((propName) => timeZone.indices[propName].indexOf(lowerCaseFilter) >= 0);
};
export const getFilteredTimezones = createSelector(
  [getTimeZones, getFilter],
  (timeZones, filter) => {
    if (!filter) {
      return timeZones;
    }

    return timeZones.filter(filterMatcher(filter));
  }
);
