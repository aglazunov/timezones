import React from 'react';
import T from 'prop-types';
import styled, { css } from 'styled-components';

const ChunkElement = styled.span`
  position: relative;
  z-index: 2;

  ${(props) =>
    props.matches &&
    css`
      font-weight: bold;
      z-index: 1;
      color: black;
      padding: 0 4px;
      margin: 0 -4px;
      cursor: pointer;
      white-space: pre-wrap;
      border-radius: 4px;
      background: #fcef5b;
      box-shadow: inset 1px 0 #b3b3b3, inset 0 1px #b3b3b3, inset -1px 0 #b3b3b3,
        inset 0 -1px #b3b3b3;
    `}
`;

const MatchVisualization = ({ phrase, chunks }) => {
  if (!chunks) {
    return phrase;
  }

  return (
    <>
      {chunks.map(({ start, end, matches }, index) => (
        <ChunkElement key={String(index)} matches={matches}>
          {phrase.substring(start, end)}
        </ChunkElement>
      ))}
    </>
  );
};

MatchVisualization.propTypes = {
  phrase: T.string.isRequired,
  chunks: T.arrayOf(
    T.shape({
      start: T.number.isRequired,
      end: T.number.isRequired,
      matches: T.bool.isRequired,
    })
  ),
};

MatchVisualization.defaultProps = {
  chunks: null,
};

export default MatchVisualization;
