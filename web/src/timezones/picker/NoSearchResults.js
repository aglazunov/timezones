import React from 'react';
import { Message } from 'semantic-ui-react';

const NoSearchResults = () => (
  <Message
    warning
    header="No matches"
    content="Try removing a few trailing characters from the search field."
  />
);

export default NoSearchResults;
