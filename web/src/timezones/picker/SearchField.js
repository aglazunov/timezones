import React, { useContext, useMemo } from 'react';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';

import SearchContext from './SearchContext';
import useAutofocus from '../../hooks/useAutoFocus';
import useThrottling from '../../hooks/useThrottling';
import { SET_FILTER } from './reducer/constants';

const InputElement = styled.input`
  background: rgba(255, 255, 255, 0.7) !important;
`;

const SearchField = () => {
  const { dispatch } = useContext(SearchContext);
  const inputRef = useAutofocus();
  const dispatcher = useMemo(
    () => (value) => {
      dispatch({
        type: SET_FILTER,
        filter: value,
      });
    },
    [dispatch]
  );

  const scheduleSetFilter = useThrottling(dispatcher);

  const onSearch = (event) => {
    const { value } = event.target;

    scheduleSetFilter(value);
  };

  // we willingly go for uncontrolled component to provide a more responsive UI
  return (
    <>
      <div className="ui icon input fluid">
        <Icon name="search" aria-hidden="true" />
        <InputElement
          data-cy="searchField"
          className="ui fluid"
          ref={inputRef}
          placeholder="Type to search"
          onChange={onSearch}
        />
      </div>
    </>
  );
};

export default SearchField;
