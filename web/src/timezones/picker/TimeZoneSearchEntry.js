import React from 'react';
import styled from 'styled-components';
import T from 'prop-types';
import { Label } from 'semantic-ui-react';

import { timeZonePropType } from '../../propTypes';
import MatchVisualization from './MatchVisualization';
import Box from '../../components/layout/Box';
import Flex from '../../components/layout/Flex';
import TimeZoneDifference from '../common/TimeZoneDifference';
import CountryFlag from '../common/CountryFlag';

const TimeZoneSearchListItem = styled(Box)`
  background: rgba(129, 135, 150, 0.4);

  &:hover {
    background: rgba(129, 135, 150, 0.6);
  }
`;

const Text = styled.span`
  position: relative;
  z-index: 2;
`;

const TimeZoneSearchEntry = ({ entry, meta, onClick }) => {
  const { matchedMajorProperties = {}, matchedAuxProperties = {} } = meta;
  const diffColor = entry.current ? 'yellow' : 'teal';

  return (
    <TimeZoneSearchListItem px={2} py={1} current={entry.current} onClick={onClick}>
      <Flex alignItems="center">
        <Box fontSize={2} flexGrow={1}>
          <MatchVisualization phrase={entry.name} chunks={matchedMajorProperties.name} />
        </Box>
        <Label size="tiny" color="grey">
          <MatchVisualization
            phrase={entry.abbreviation}
            chunks={matchedMajorProperties.abbreviation}
          />
        </Label>
        <Label color={diffColor} size="small">
          GMT
          <TimeZoneDifference timeZone={entry} />
        </Label>
      </Flex>
      {matchedAuxProperties.alternativeName && (
        <Box>
          <MatchVisualization
            phrase={entry.alternativeName}
            chunks={matchedAuxProperties.alternativeName}
          />
        </Box>
      )}
      {(matchedAuxProperties.countryName || matchedAuxProperties.countryCode) && (
        <Box>
          <CountryFlag timeZone={entry} />
          <MatchVisualization
            phrase={entry.countryName}
            chunks={matchedAuxProperties.countryName}
          />
          <Text> (</Text>
          <MatchVisualization
            phrase={entry.countryCode}
            chunks={matchedAuxProperties.countryCode}
          />
          <Text>)</Text>
        </Box>
      )}
    </TimeZoneSearchListItem>
  );
};

TimeZoneSearchEntry.propTypes = {
  entry: timeZonePropType.isRequired,
  meta: T.shape({
    matchedAuxProperties: T.object,
    matchedMajorProperties: T.object,
  }).isRequired,
  onClick: T.func,
};

TimeZoneSearchEntry.defaultProps = {
  onClick: null,
};

export default TimeZoneSearchEntry;
