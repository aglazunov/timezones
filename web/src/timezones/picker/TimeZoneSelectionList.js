import React, { useContext } from 'react';

import T from 'prop-types';
import { Message } from 'semantic-ui-react';
import SearchContext from './SearchContext';
import { getFilter, getSearchResult, getTotalTimeZones } from './reducer/selectors';
import TimeZoneSearchEntry from './TimeZoneSearchEntry';
import NoSearchResults from './NoSearchResults';
import Group from '../../components/layout/Group';
import Box from '../../components/layout/Box';
import FilteredNote from '../../list/FilteredNote';

const EMPTY_META = {};

const TimeZoneSelectionList = ({ onSelect }) => {
  const { state } = useContext(SearchContext);
  const filter = getFilter(state);
  const displayedZones = getSearchResult(state);
  const totalZones = getTotalTimeZones(state);

  if (displayedZones.length === 0) {
    return (
      <Box m={2}>
        <NoSearchResults />
      </Box>
    );
  }

  const handleSelectTimeZone = (timeZone) => {
    onSelect && onSelect(timeZone);
  };

  return (
    <>
      <Group vGap="1px" flexDirection="column">
        {displayedZones.map((zone) => {
          const simpleMatch = !(zone.entry instanceof Object);
          const entry = simpleMatch ? zone : zone.entry;

          return (
            <TimeZoneSearchEntry
              key={entry.name}
              entry={entry}
              meta={simpleMatch ? EMPTY_META : zone.meta}
              onClick={() => handleSelectTimeZone(entry)}
            />
          );
        })}
      </Group>

      <Box p={2}>
        <Message>
          <FilteredNote
            total={totalZones}
            filtered={displayedZones.length}
            filterApplied={Boolean(filter)}
            noun="time zone"
          />
        </Message>
      </Box>
    </>
  );
};

TimeZoneSelectionList.propTypes = {
  onSelect: T.func,
};

TimeZoneSelectionList.defaultProps = {
  onSelect: null,
};

export default TimeZoneSelectionList;
