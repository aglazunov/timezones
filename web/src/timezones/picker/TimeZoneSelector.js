import React, { useReducer } from 'react';
import styled from 'styled-components';
import T from 'prop-types';
import { Form } from 'semantic-ui-react';

import SearchField from './SearchField';
import TimeZoneSelectionList from './TimeZoneSelectionList';
import SearchContext from './SearchContext';
import searchReducer from './reducer/searchReducer';

const initialState = searchReducer(undefined, {});

const TimeZoneSearchWrapper = styled(Form)``;

const TimeZoneSelector = ({ onSelect }) => {
  const [state, dispatch] = useReducer(searchReducer, initialState);

  return (
    <SearchContext.Provider value={{ state, dispatch }}>
      <TimeZoneSearchWrapper>
        <SearchField />
      </TimeZoneSearchWrapper>
      <TimeZoneSelectionList onSelect={onSelect} />
    </SearchContext.Provider>
  );
};

TimeZoneSelector.propTypes = {
  onSelect: T.func,
};

TimeZoneSelector.defaultProps = {
  onSelect: null,
};

export default TimeZoneSelector;
