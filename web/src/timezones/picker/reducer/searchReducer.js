import { combineReducers } from 'redux';
import { createReducer } from '../../../util/reducers';
import * as constants from './constants';

const EMPTY_FILTER = '';

const filterReducer = createReducer(
  {
    [constants.CLEAR_FILTER]: () => EMPTY_FILTER,
    [constants.SET_FILTER]: (state, { filter }) => filter,
  },
  EMPTY_FILTER
);

const reducer = combineReducers({
  filter: filterReducer,
});

export default reducer;
