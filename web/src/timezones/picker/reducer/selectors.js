import { createSelector } from 'reselect';
import { getAllTimeZones, searchTimezones } from '../search';

export const getFilter = (state) => state.filter;
export const getTotalTimeZones = () => getAllTimeZones().length;

export const getSearchResult = createSelector(getFilter, (filter) => {
  const timeZones = getAllTimeZones();

  return searchTimezones(filter, timeZones);
});
