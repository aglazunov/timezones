import { getTimeZones } from '@vvo/tzdb';

export const majorProperties = ['name', 'abbreviation'];
export const auxProperties = ['alternativeName', 'countryName', 'countryCode'];

// advancedFilter (also on aux properties) gets activated when the filter gets to a certain length.
// otherwise one-char filter will produce to many results

const AUX_FILTER_ACTIVATION_THRESHOLD = 2;
const CURRENT_TIME_ZONE = Intl.DateTimeFormat().resolvedOptions().timeZone;

const ALL_TIME_ZONES = getTimeZones().map((timeZone) => ({
  ...timeZone,
  indices: [...majorProperties, ...auxProperties].reduce((acc, prop) => {
    acc[prop] = timeZone[prop].toLowerCase();

    return acc;
  }, {}),
  current: timeZone.name === CURRENT_TIME_ZONE,
}));

export const getAllTimeZones = () => ALL_TIME_ZONES;
const timeZoneMap = ALL_TIME_ZONES.reduce((acc, timeZone) => {
  acc[timeZone.name] = timeZone;

  return acc;
}, {});

// this function splits the string into the alternating portions of matching or not matching the substring
export const splitByMatch = (str, match) => {
  let position = -1;
  let cursor = 0;
  const result = [];

  const addPosition = (start, end, matches) => {
    if (start !== end) {
      result.push({
        start,
        end,
        matches,
      });
    }
  };

  do {
    if (position >= 0) {
      addPosition(cursor, position, false);
      cursor = position + match.length;
      addPosition(position, cursor, true);
    }

    position = str.indexOf(match, position + 1);
  } while (position >= 0);

  if (cursor < str.length) {
    addPosition(cursor, str.length, false);
  }

  return result;
};

// function that will return whether a timezone got matched
// return format:
//  * timezone entry if matched only on major properties
//  * extended object if matched on aux properties
export const timezoneMatch = (filter = '') => (timeZone) => {
  const filterString = filter.trim().toLowerCase();

  const matchesProperty = (property) => timeZone.indices[property].indexOf(filterString) >= 0;
  const getMatchMap = (properties) =>
    properties.reduce((acc, prop) => {
      acc[prop] = splitByMatch(timeZone.indices[prop], filterString);

      return acc;
    }, {});

  if (filterString.length === 0) {
    return timeZone;
  }

  const matchedAuxPropList = auxProperties.filter(matchesProperty);
  const matchedMajorPropList = majorProperties.filter(matchesProperty);
  const matchedAuxProperties = getMatchMap(matchedAuxPropList);
  const matchedMajorProperties = getMatchMap(matchedMajorPropList);
  const matchesMajor = matchedMajorPropList.length > 0;
  const matchesAux = matchedAuxPropList.length > 0;

  if (filterString.length < AUX_FILTER_ACTIVATION_THRESHOLD) {
    // if major properties match, return timezone, false otherwise
    return matchesMajor && timeZone;
  }

  if (!matchesMajor && !matchesAux) {
    return false;
  }

  // matches aux and maybe major
  return {
    entry: timeZone,
    meta: {
      matchedAuxProperties,
      matchedMajorProperties,
    },
  };
};

export const searchTimezones = (filter = '', timeZones) => {
  const matchFunction = timezoneMatch(filter);

  return timeZones.map(matchFunction).filter(Boolean);
};

export const getTimeZoneByName = (name) => timeZoneMap[name];
