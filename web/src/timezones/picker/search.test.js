/* eslint no-console: 0 */

import { getAllTimeZones, searchTimezones, splitByMatch } from './search';

test('searches the timezones', () => {
  const timeZones = getAllTimeZones();
  const result = searchTimezones('CET', timeZones);

  expect(result).toHaveLength(33);
});

const visualizeChunks = (phrase, chunks) =>
  chunks
    .map(
      ({ start, end, matches }) =>
        `${matches ? '[' : ''}${phrase.substring(start, end)}${matches ? ']' : ''}`
    )
    .join('');

test('splitByMatch', () => {
  const testData = [
    {
      phrase: 'We all live in a yellow submarine',
      match: 'in',
      result: [
        { start: 0, end: 12, matches: false },
        { start: 12, end: 14, matches: true },
        { start: 14, end: 30, matches: false },
        { start: 30, end: 32, matches: true },
        { start: 32, end: 33, matches: false },
      ],
    },
    {
      phrase: 'hello',
      match: 'search',
      result: [{ start: 0, end: 5, matches: false }],
    },
    {
      phrase: 'ababa',
      match: 'ab',
      result: [
        { start: 0, end: 2, matches: true },
        { start: 2, end: 4, matches: true },
        { start: 4, end: 5, matches: false },
      ],
    },
    {
      phrase: 'whole term',
      match: 'whole term',
      result: [{ start: 0, end: 10, matches: true }],
    },
  ];

  testData.forEach(({ phrase, match, result }) => {
    const actualResult = splitByMatch(phrase, match);

    console.log('actualResult', actualResult);

    console.log(`Visualizing for phrase ${phrase} and term ${match}`);
    console.log(visualizeChunks(phrase, actualResult));

    expect(actualResult).toEqual(result);
  });
});
