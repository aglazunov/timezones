/* eslint-disable no-console */

import { getTimeZones } from '@vvo/tzdb';
import { getAllTimeZones } from './picker/search';

test('ensure there are no weird offsets (not in 15 minute steps)', () => {
  const timeZones = getAllTimeZones();
  const weirdZones = timeZones.filter((timeZone) => timeZone.currentTimeOffsetInMinutes % 15 !== 0);

  // console.log(weirdZones.map((timeZone) => timeZone.currentTimeOffsetInMinutes));

  expect(weirdZones).toHaveLength(0);
});

test('ensure there are at least some main cities present for each time zone', () => {
  const timeZones = getAllTimeZones();
  const weirdZones = timeZones.filter(
    (timeZone) => !(timeZone.mainCities instanceof Array) || timeZone.mainCities.length < 1
  );

  expect(weirdZones).toHaveLength(0);
});

test('ensure all zones have country, code, name and alternative name', () => {
  const timeZones = getAllTimeZones();
  const weirdZones = timeZones.filter(
    ({ name, alternativeName, countryName, countryCode }) =>
      !name || !alternativeName || !countryName || !countryCode
  );

  expect(weirdZones).toHaveLength(0);
});

test('generate max length outliers', () => {
  const timeZones = getTimeZones();
  const filter = (entry) => entry.name.indexOf('Berlin') >= 0;
  // filter = (entry) => typeof entry.alternativeName !== 'string';
  const filteredTz = timeZones.filter(filter);

  const props = ['name', 'alternativeName', 'mainCities'];

  props.forEach((prop) => {
    // stringify prop
    const str = (value) => {
      if (typeof value === 'string') {
        return value;
      }

      if (value instanceof Array) {
        return String(value);
      }

      return '';
    };

    const outlier = timeZones.reduce((result, timeZone) => {
      if (!result || str(timeZone[prop]).length > str(result[prop]).length) {
        return timeZone;
      }

      return result;
    }, null);

    const longString = str(outlier[prop]);
    console.log(
      `outlier for the property ${prop} is ${longString} – of length ${longString.length}`
    );
  });

  console.log(filteredTz);
});
