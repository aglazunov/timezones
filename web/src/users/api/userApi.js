import axios from 'axios';
import { API_ENDPOINT } from '../../config';

export const loadUserProfile = (userId) => axios.get(`${API_ENDPOINT}/users/${userId}`);

export const loadUsers = () => axios.get(`${API_ENDPOINT}/users`);

export const updateUser = (userId, values) =>
  axios.patch(`${API_ENDPOINT}/users/${userId}`, values);

export const deleteUser = (userId) => axios.delete(`${API_ENDPOINT}/users/${userId}`);
