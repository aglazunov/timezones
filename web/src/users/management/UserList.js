import React, { useContext } from 'react';
import T from 'prop-types';

import Group from '../../components/layout/Group';
import { timeZonePropType } from '../../propTypes';
import UserListItem from './UserListItem';
import AuthContext from '../../app/AuthContext';
import { isPermitted, PermissionTypes } from '../../integration/permissions';

const UserList = ({ users }) => {
  const { user } = useContext(AuthContext);
  const ownRecordsPermitted = isPermitted(PermissionTypes.ManageOwnUser, user.permissionLevel);
  const otherRecordsPermitted = isPermitted(
    PermissionTypes.ManageOtherRecords,
    user.permissionLevel
  );

  return (
    <Group vGap="1px" flexDirection="column">
      {users.map(({ firstName, lastName, email, id }) => (
        <UserListItem
          useLinks={email === user.email ? ownRecordsPermitted : otherRecordsPermitted}
          key={id}
          firstName={firstName}
          lastName={lastName}
          email={email}
          id={id}
        />
      ))}
    </Group>
  );
};

UserList.propTypes = {
  users: T.arrayOf(timeZonePropType).isRequired,
};

export default UserList;
