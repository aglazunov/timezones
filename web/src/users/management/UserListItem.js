import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import T from 'prop-types';

import { ListItemBox, ListItemLink } from '../../list/elements';
import Box from '../../components/layout/Box';
import Flex from '../../components/layout/Flex';

const UserListItem = ({ email, firstName, lastName, id, useLinks }) => {
  const WrapperComponent = useLinks ? ListItemLink : ListItemBox;

  return (
    <WrapperComponent to={useLinks ? `/users/${id}/time-zones` : undefined}>
      <Flex>
        <Box flexGrow={1}>
          <Box fontSize={2}>{email}</Box>
          <Box>
            {firstName} {lastName}
          </Box>
        </Box>
        <Box flexShrink={0}>
          <Button as={Link} primary icon="edit" to={`/users/${id}/profile`} />
        </Box>
      </Flex>
    </WrapperComponent>
  );
};

UserListItem.propTypes = {
  email: T.string.isRequired,
  firstName: T.string.isRequired,
  lastName: T.string.isRequired,
  id: T.string.isRequired,
  useLinks: T.bool,
};

UserListItem.defaultProps = {
  useLinks: true,
};

export default UserListItem;
