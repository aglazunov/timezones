import React, { useEffect, useReducer } from 'react';
import { Loader, Message } from 'semantic-ui-react';

import Box from '../../components/layout/Box';
import * as api from '../api/userApi';
import useDataLoading from '../../hooks/useDataLoading';
import reducer from './redux/reducer';
import { getFilter, getUsers, getFilteredUsers } from './redux/selectors';
import { LOAD_DATA_ACTION_TYPE, SET_FILTER_ACTION_TYPE } from './redux/actionTypes';
import { statusChecks } from '../../util/statuses';
import Flex from '../../components/layout/Flex';
import ListFilter from '../../list/ListFilter';
import UserList from './UserList';
import FilteredNote from '../../list/FilteredNote';

const initialState = reducer(undefined, {});

const UserManagement = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const users = getUsers(state);
  const filter = getFilter(state);
  const { loadingState } = state;

  const usersAvailable = users instanceof Array && users.length > 0;
  const filteredUsers = getFilteredUsers(state);

  const fetchData = useDataLoading(LOAD_DATA_ACTION_TYPE, api.loadUsers, dispatch);
  useEffect(fetchData, [fetchData]);

  const handleFilterChange = (value) => {
    dispatch({
      type: SET_FILTER_ACTION_TYPE,
      filter: value,
    });
  };

  return (
    <Box flex="1 0 auto">
      <Box mb={3} mt={3} pl={3} fontSize={3}>
        User Management
      </Box>
      {statusChecks.isError(loadingState.status) && (
        <Box px={3}>
          <Message error content={loadingState.message} />
        </Box>
      )}
      {statusChecks.isInProgress(loadingState.status) && (
        <Flex justifyContent="center" mb={3}>
          <Loader active inline />
        </Flex>
      )}
      {statusChecks.isSuccess(loadingState.status) && usersAvailable && (
        <>
          <ListFilter onChange={handleFilterChange} />
          <UserList users={filteredUsers} />

          {filter && (
            <Box p={3}>
              <Message>
                <FilteredNote
                  total={users.length}
                  filtered={filteredUsers.length}
                  filterApplied={Boolean(filter)}
                  noun="user"
                />
              </Message>
            </Box>
          )}
        </>
      )}
    </Box>
  );
};

export default UserManagement;
