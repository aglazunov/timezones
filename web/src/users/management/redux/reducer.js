import { combineReducers } from 'redux';
import sortOn from 'sort-on';

import { createReducer, loadingStateReducer, statusActionType } from '../../../util/reducers';
import * as statuses from '../../../util/statuses';
import { LOAD_DATA_ACTION_TYPE, SET_FILTER_ACTION_TYPE } from './actionTypes';

const reducer = combineReducers({
  loadingState: loadingStateReducer(LOAD_DATA_ACTION_TYPE),
  users: createReducer(
    {
      [statusActionType(LOAD_DATA_ACTION_TYPE, statuses.SUCCESS)]: (state, { data }) =>
        sortOn(
          data.map((user) => ({
            ...user,
            id: user._id,
          })),
          ['email']
        ),
    },
    null
  ),
  filter: createReducer(
    {
      [SET_FILTER_ACTION_TYPE]: (state, { filter }) => filter,
    },
    null
  ),
});

export default reducer;
