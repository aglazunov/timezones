import { createSelector } from 'reselect';

export const getUsers = (state) => state.users;
export const getFilter = (state) => state.filter;

const properties = ['firstName', 'lastName', 'email'];

const filterMatcher = (filter) => (user) => {
  const lowerCaseFilter = filter.toLowerCase();

  return properties.some((propName) => user[propName].toLowerCase().indexOf(lowerCaseFilter) >= 0);
};

export const getFilteredUsers = createSelector([getUsers, getFilter], (users, filter) => {
  if (!filter) {
    return users;
  }

  return users.filter(filterMatcher(filter));
});
