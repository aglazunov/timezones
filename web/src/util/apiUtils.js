export const getErrorMessage = (error, defaultMessage) => {
  let message;

  if (typeof error === 'string') {
    message = error;
  }

  if (
    error instanceof Object &&
    error.response instanceof Object &&
    error.response.data instanceof Object
  ) {
    ({ message } = error.response.data);
  }

  if (!message) {
    ({ message } = error);
  }

  if (message && defaultMessage) {
    return `${defaultMessage}: ${message}`;
  }

  return message || defaultMessage;
};
