import * as statuses from './statuses';

const SEPARATOR = '_';

export const createReducer = (fnMap, initialState) => (state = initialState, payload) => {
  const handler = fnMap[payload.type];

  return handler ? handler(state, payload) : state;
};

const defaultLoadingState = {
  status: statuses.INITIAL,
  message: undefined,
};

export const statusActionType = (baseAction, status) => [baseAction, status].join(SEPARATOR);

export const statusAction = (baseAction, status, params) => ({
  type: statusActionType(baseAction, status),
  ...params,
});

const constructLoadingState = (status) => (state, action) => ({
  status,
  message: action.message || null,
});

export const loadingStateReducer = (baseAction) =>
  createReducer(
    {
      [statusActionType(baseAction, statuses.IN_PROGRESS)]: constructLoadingState(
        statuses.IN_PROGRESS
      ),
      [statusActionType(baseAction, statuses.SUCCESS)]: constructLoadingState(statuses.SUCCESS),
      [statusActionType(baseAction, statuses.ERROR)]: constructLoadingState(statuses.ERROR),
    },
    defaultLoadingState
  );

export const enhanceWithResetActions = (resetActions, propagateResetActionToReducer) => {
  const isResetAction = (action) => resetActions.includes(action.type);

  return (reducer) => (state, action) => {
    let currentState = state;

    if (isResetAction(action)) {
      currentState = reducer(undefined, action);

      if (!propagateResetActionToReducer) {
        return currentState;
      }
    }

    return reducer(currentState, action);
  };
};
