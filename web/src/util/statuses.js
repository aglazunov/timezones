export const INITIAL = 'INITIAL';
export const SUCCESS = 'SUCCESS';
export const IN_PROGRESS = 'IN_PROGRESS';
export const ERROR = 'ERROR';

export const statusChecks = {
  isError: (status) => status === ERROR,
  isInitial: (status) => status === INITIAL,
  isInProgress: (status) => status === IN_PROGRESS,
  isSuccess: (status) => status === SUCCESS,
};
